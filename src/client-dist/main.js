(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-sidenav-container>\n    <mat-sidenav #sidenav class=\"sidenav\" mode=\"push\">\n        <mat-nav-list>\n\n            <a mat-list-item routerLink=\"/\" (click)=\"sidenav.close()\">Home</a>\n            <a mat-list-item routerLink=\"/login\" (click)=\"sidenav.close()\" *ngIf=\"!(userService.currentUser | async)\">Login</a>\n            <a mat-list-item routerLink=\"/register\" (click)=\"sidenav.close()\" *ngIf=\"!(userService.currentUser | async)\">Register</a>\n            <a mat-list-item routerLink=\"/user/dashboard\" (click)=\"sidenav.close()\" *ngIf=\"(userService.currentUser | async)\">User Dashboard</a>\n            <a mat-list-item routerLink=\"/admin/dashboard\" (click)=\"sidenav.close()\" *ngIf=\"(userService.isAdmin() | async)\">Admin Dashboard</a>\n            <a mat-list-item routerLink=\"/user/settings\" *ngIf=\"(userService.currentUser | async)\" (click)=\"sidenav.close()\">Account Settings</a>\n            <a mat-list-item (click)=\"logout(); sidenav.close()\" *ngIf=\"(userService.currentUser | async)\">Logout</a>\n        \n        </mat-nav-list>\n    </mat-sidenav>\n    <mat-sidenav-content>\n        <mat-toolbar style=\"background-color: #ffffff; border-bottom: 1px solid #CCCCCC;\">\n            <button mat-icon-button (click)=\"sidenav.toggle()\">\n                <mat-icon>menu</mat-icon>\n            </button>\n            <span class=\"spacer\"></span>\n            <button class=\"center_icon\" mat-button *ngIf=\"(userService.currentUser | async)\" routerLink=\"/user/dashboard\">\n                <span>{{ (userService.currentUser | async).Username }}</span>\n            </button>\n        </mat-toolbar>\n        <main>\n            <router-outlet></router-outlet>\n        </main>\n    </mat-sidenav-content>\n</mat-sidenav-container>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/admin/dashboard/dashboard.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/admin/dashboard/dashboard.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <h1>Admin Dashboard</h1>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <h2>All Posts</h2>\n        </div>\n        <div class=\"col-sm-12\" *ngIf=\"myPosts\">\n            <div style=\"margin: 10px 0;\">\n                <button mat-raised-button color=\"primary\"[routerLink]=\"['/post/edit']\">\n                    Create post\n                </button>\n            </div>\n\n            <table mat-table [dataSource]=\"myPosts\" class=\"mat-elevation-z8 userPosts\">\n                <ng-container matColumnDef=\"index\">\n                    <th mat-header-cell *matHeaderCellDef> No. </th>\n                    <td mat-cell *matCellDef=\"let element; let i = index;\">{{i + 1}}</td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"title\">\n                    <th mat-header-cell *matHeaderCellDef> Title </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.Title}} </td>\n                </ng-container>\n                \n                <ng-container matColumnDef=\"date\">\n                    <th mat-header-cell *matHeaderCellDef> Date </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{ convertToString(element.Date) }} </td>\n                </ng-container>\n                \n                <ng-container matColumnDef=\"status\">\n                    <th mat-header-cell *matHeaderCellDef> Status </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.MD_Active === 0 ? 'Deleted' : element.Status === 0 ? 'Hidden' : 'Published'}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"edit\">\n                    <th mat-header-cell *matHeaderCellDef> Settings </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <button *ngIf=\"element.MD_Active\" mat-raised-button color=\"primary\"[routerLink]=\"['/post/edit/', element.UID]\" style=\"margin-right: 8px;\">\n                            Edit\n                        </button>\n\n                        <button *ngIf=\"element.MD_Active\" mat-raised-button color=\"warn\"(click)=\"deletePost($event, element.UID)\" style=\"margin-right: 8px;\">\n                            Delete\n                        </button>\n\n                        <button *ngIf=\"element.MD_Active && element.Status == 0\" mat-raised-button color=\"primary\" (click)=\"toggleStatus($event, element.UID)\">\n                            Publish\n                        </button>\n\n                        <button *ngIf=\"element.MD_Active && element.Status == 1\" mat-raised-button color=\"primary\" (click)=\"toggleStatus($event, element.UID)\">\n                            Hide\n                        </button>\n                    </td>\n                </ng-container>\n                \n                <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                <tr mat-row [routerLink]=\"['/post/detail/', row.UID]\" class=\"\"  *matRowDef=\"let row; columns: displayedColumns; let i = index\"></tr>\n            </table>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/home/home.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/home/home.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wrapper\">\n    <div class=\"posts\" *ngIf=\"allPosts\">\n        <mat-card *ngIf=\"allPosts.length == 0\" style=\"min-height: 68px; text-align: center; vertical-align:middle;\">\n            <p>Be the first one to create a post!</p>\n        </mat-card>\n        <mat-card class=\"post\" *ngFor=\"let post of allPosts\" [routerLink]=\"['/post/detail/', post.UID]\">\n            <mat-card-header>\n                <mat-card-title>{{ post.Title }}</mat-card-title>\n                <mat-card-subtitle>{{ post.Username }}</mat-card-subtitle>\n            </mat-card-header>\n            <mat-card-content>\n                <p>{{ post.Content }}</p>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div class=\"actions\">\n        <mat-card>\n            <button mat-raised-button color=\"primary\" [routerLink]=\"(userService.currentUser | async) ? '/post/edit' : ''\">\n                Add Post\n            </button>\n        </mat-card>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/login/login.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/login/login.component.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n    <mat-card>\n        <mat-card-title *ngIf=\"!tfaActive\">Login</mat-card-title>\n        <mat-card-title *ngIf=\"tfaActive\">TFA Token</mat-card-title>\n        <mat-card-content>\n            <form [formGroup]=\"form\" (ngSubmit)=\"submit()\" *ngIf=\"!tfaActive\">\n                <div class=\"row\">\n                    <div class=\"col-sm-12\">\n                        <mat-form-field>\n                            <input type=\"text\" matInput placeholder=\"Username\" formControlName=\"username\">\n                            <mat-error *ngIf=\"hasError('username', 'required')\">Username is required</mat-error>\n                        </mat-form-field>\n                    </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-sm-12\">\n                        <mat-form-field>\n                            <input type=\"password\" matInput placeholder=\"Password\" formControlName=\"password\">\n                            <mat-error *ngIf=\"hasError('password', 'required')\">Password is required</mat-error>\n                        </mat-form-field>\n                    </div>\n                </div>\n                <div class=\"buttons\">\n                    <button type=\"button\" mat-raised-button color=\"secondary\" [routerLink]=\"['/register']\">Register</button>\n                    <button type=\"submit\" mat-raised-button color=\"primary\">Login</button>\n                </div>\n            </form>\n            <form [formGroup]=\"tokenForm\" (ngSubmit)=\"submit()\" *ngIf=\"tfaActive\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-4\">\n                            <mat-form-field>\n                                <input type=\"text\" matInput placeholder=\"Token\" formControlName=\"token\">\n                            </mat-form-field>\n                        </div>\n                    </div>\n                <div class=\"row\">\n                    <div class=\"col-sm-12\">\n                        <button type=\"submit\" mat-raised-button color=\"primary\" [disabled]=\"!tokenForm.valid\">Verify Token</button>\n                    </div>\n                </div>\n            </form>\n        </mat-card-content>\n    </mat-card>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/post/detail/detail.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/post/detail/detail.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n        <button mat-raised-button color=\"primary\" (click)=\"location.back()\">\n            <mat-icon>arrow_back</mat-icon>\n            Back\n        </button>\n    <!-- Create -->\n    <mat-card *ngIf=\"post\">\n        <mat-card-header>\n            <!-- <div mat-card-avatar class=\"avatar-image\"></div> -->\n            <mat-card-title>{{ post.Title }}</mat-card-title>\n            <mat-card-subtitle>{{ post.Username }}</mat-card-subtitle>\n        </mat-card-header>\n        <!-- <img mat-card-image src=\"https://material.angular.io/assets/img/examples/shiba2.jpg\" alt=\"Photo of a Shiba Inu\"> -->\n        <mat-card-content>\n            <p>{{ post.Content }}</p>\n        </mat-card-content>\n    </mat-card>\n</div>\n<div class=\"container\">\n    <mat-card *ngIf=\"post && post.Comments\">\n        <mat-card-header>\n            <mat-card-title>Comments</mat-card-title>\n        </mat-card-header>\n        <mat-card-content>\n            <div *ngIf=\"userService.isLoggedIn()\">\n                <form [formGroup]=\"commentForm\" (ngSubmit)=\"postComment()\">\n                    <mat-form-field>\n                        <textarea matInput formControlName=\"comment\"></textarea>\n                        <mat-error *ngIf=\"commentForm.controls['comment']?.hasError('minlength')\">Comment to short!</mat-error>\n                    </mat-form-field>\n                    <div style=\"display: flex; justify-content: flex-end;\">\n                        <button type=\"submit\" mat-flat-button color=\"primary\" [disabled]=\"commentForm.invalid\">Comment</button>\n                    </div>\n                </form>\n            </div>\n            <div *ngIf=\"!userService.isLoggedIn()\" class=\"margin\">\n                <span>Log in to comment</span>\n            </div>\n\n            <span class=\"margin\" *ngIf=\"post.Comments.length === 0\">No comments yet</span>\n            <div class=\"comments\">\n                <div class=\"comment\" *ngFor=\"let comment of post.Comments\">\n                    <h3>{{ comment.Username }} <small>{{ convertToString(comment.Date) }}</small></h3>\n                    <p>{{ comment.Content }}</p>\n                </div>\n            </div>\n        </mat-card-content>\n    </mat-card>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/post/edit/edit.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/post/edit/edit.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n        <button mat-raised-button color=\"primary\" (click)=\"location.back()\">\n            <mat-icon>arrow_back</mat-icon>\n            Back\n        </button>\n    <!-- Create -->\n    <mat-card>\n        <mat-card-title>\n            {{ isEditMode ? 'Edit Post' : 'Create Post'}}\n        </mat-card-title>\n        <mat-card-content>\n            <form [formGroup]=\"form\">\n                <mat-form-field>\n                    <input matInput placeholder=\"Title\" formControlName=\"Title\">\n                </mat-form-field>\n    \n                <mat-form-field>\n                    <textarea matInput placeholder=\"Content\" formControlName=\"Content\"></textarea>\n                </mat-form-field>\n    \n                <button mat-raised-button color=\"primary\" (click)=\"isEditMode ? savePost() : createPost()\" [disabled]=\"!form.valid\">\n                    <mat-icon>{{ isEditMode ? 'save' : 'add_box'}}</mat-icon>\n                    {{ isEditMode ? 'Save Post' : 'Create Post'}}\n                </button>\n            </form>\n        </mat-card-content>\n    </mat-card>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/register/register.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/register/register.component.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n    <mat-card>\n        <mat-card-title>Register</mat-card-title>\n        <mat-card-content>\n            <form [formGroup]=\"form\" (ngSubmit)=\"submit()\">\n                <div class=\"row\">\n                    <div class=\"col-sm-12\">\n                        <mat-form-field>\n                            <input type=\"text\" matInput placeholder=\"Name\" formControlName=\"name\" [errorStateMatcher]=\"showImmediate\">\n                            <mat-error *ngIf=\"hasError('name', 'required')\">Name is required</mat-error>\n                            <mat-error *ngIf=\"hasError('name', 'maxlength')\">Name is too long</mat-error>\n                        </mat-form-field>\n                    </div>\n                </div>\n                \n                <div class=\"row\">\n                    <div class=\"col-sm-12\">\n                        <mat-form-field>\n                            <input type=\"text\" matInput placeholder=\"Username\" formControlName=\"username\" [errorStateMatcher]=\"showImmediate\">\n                            <mat-error *ngIf=\"hasError('username', 'required')\">Username is required</mat-error>\n                            <mat-error *ngIf=\"hasError('username', 'maxlength')\">Username is too long</mat-error>\n                            <mat-error *ngIf=\"hasError('username', 'usernameTaken')\">Username already taken!</mat-error>\n                        </mat-form-field>\n                    </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-sm-12\">\n                        <mat-form-field>\n                            <input type=\"password\" matInput placeholder=\"Password\" formControlName=\"password\">\n                            <mat-error *ngIf=\"hasError('password', 'required')\">Password is required</mat-error>\n                        </mat-form-field>\n                    </div>\n                    <div class=\"col-sm-12\">\n                        <mat-form-field>\n                            <input type=\"password\" matInput placeholder=\"Confirm Password\" formControlName=\"confirmPassword\" [errorStateMatcher]=\"showAfterTouched\">\n                            <mat-error *ngIf=\"form.hasError('notSame')\">Passwords don't match</mat-error>\n                        </mat-form-field>\n                    </div>\n                </div>\n                <div class=\"buttons\">\n                    <button type=\"button\" mat-raised-button color=\"secondary\" [routerLink]=\"['/login']\">Log in</button>\n                    <button type=\"submit\" mat-raised-button color=\"primary\">Register</button>\n                </div>\n            </form>\n        </mat-card-content>\n    </mat-card>\n</div>\n    ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/confirm/confirm.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/confirm/confirm.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1 mat-dialog-title>Confirm action</h1>\n<div mat-dialog-content>\n    <p>Are you sure you want to continue with this action?</p>\n</div>\n<div mat-dialog-actions>\n    <button mat-button (click)=\"onNoClick()\">Cancel</button>\n    <button mat-button [mat-dialog-close]=\"true\" cdkFocusInitial>Yes</button>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/dashboard/dashboard.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/dashboard/dashboard.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <h1>User Dashboard</h1>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <h2>Your Posts</h2>\n        </div>\n        <div class=\"col-sm-12\" *ngIf=\"myPosts\">\n            <div style=\"margin: 10px 0;\">\n                <button mat-raised-button color=\"primary\" [routerLink]=\"['/post/edit']\">\n                    Create post\n                </button>\n            </div>\n            <table mat-table [dataSource]=\"myPosts\" style=\"width: 100%;\" class=\"mat-elevation-z8\">\n                <ng-container matColumnDef=\"index\">\n                    <th mat-header-cell *matHeaderCellDef> No. </th>\n                    <td mat-cell *matCellDef=\"let element; let i = index;\">{{i + 1}}</td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"title\">\n                    <th mat-header-cell *matHeaderCellDef> Title </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.Title}} </td>\n                </ng-container>\n                \n                <ng-container matColumnDef=\"date\">\n                    <th mat-header-cell *matHeaderCellDef> Date </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{ convertToString(element.Date) }} </td>\n                </ng-container>\n                \n                <ng-container matColumnDef=\"status\">\n                    <th mat-header-cell *matHeaderCellDef> Status </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.MD_Active === 0 ? 'Deleted' : element.Status === 0 ? 'Hidden' : 'Published'}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"edit\">\n                    <th mat-header-cell *matHeaderCellDef> Settings </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <button mat-raised-button color=\"primary\" [routerLink]=\"['/post/edit/', element.UID]\">Edit</button>\n                        <button mat-raised-button color=\"warn\" style=\"margin-left: 8px;\" (click)=\"deletePost($event, element.UID)\">Delete</button>\n                    </td>\n                </ng-container>\n                \n                <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                <tr mat-row [routerLink]=\"['/post/detail/', row.UID]\" *matRowDef=\"let row; columns: displayedColumns; let i = index\"></tr>\n            </table>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/settings/settings.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/settings/settings.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\" *ngIf=\"userInfo\">\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <h1>User Settings</h1>\n        </div>\n    </div>\n    <form (ngSubmit)=\"saveUserData()\" >\n        <div class=\"row\">\n            <div class=\"col-sm-4\">\n                <mat-form-field>\n                    <input type=\"text\" matInput [(ngModel)]=\"userInfo.Name\" placeholder=\"Name\" name=\"name\">\n                </mat-form-field>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-sm-4\">\n                <mat-form-field>\n                    <input type=\"text\" matInput [value]=\"userInfo.Username\" placeholder=\"Username\" [disabled]=\"true\" name=\"username\">\n                </mat-form-field>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <button type=\"submit\" mat-raised-button color=\"primary\">Save</button>\n            </div>\n        </div>\n    </form>\n\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <h1>2FA</h1>\n        </div>\n    </div>\n    <form (ngSubmit)=\"submitPhoneForm()\" [formGroup]=\"phoneForm\" *ngIf=\"!tfaActive\">\n            <div class=\"row\">\n                <div class=\"col-sm-4\">\n                    <mat-form-field>\n                        <input type=\"text\" matInput placeholder=\"41791112233\" formControlName=\"phone\">\n                    </mat-form-field>\n                </div>\n            </div>\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <button type=\"submit\" mat-raised-button color=\"primary\" [disabled]=\"!phoneForm.valid && !userInfo.PhoneConfirmed\">Send Token</button>\n            </div>\n        </div>\n    </form>\n    <form (ngSubmit)=\"submitTokenForm()\" [formGroup]=\"tokenForm\" *ngIf=\"tfaActive\">\n            <div class=\"row\">\n                <div class=\"col-sm-4\">\n                    <mat-form-field>\n                        <input type=\"text\" matInput placeholder=\"Token\" formControlName=\"token\">\n                    </mat-form-field>\n                </div>\n            </div>\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <button type=\"submit\" mat-raised-button color=\"primary\" [disabled]=\"!tokenForm.valid\">Verify Token</button>\n            </div>\n        </div>\n    </form>\n    <div *ngIf=\"(userInfo.PhoneConfirmed)\" style=\"margin-top: 2rem;\">\n        <button mat-raised-button color=\"warn\" (click)=\"disableTFA()\">Disable TFA</button>\n    </div>\n\n</div>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_register_register_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/register/register.component */ "./src/app/components/register/register.component.ts");
/* harmony import */ var _components_user_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/user/dashboard/dashboard.component */ "./src/app/components/user/dashboard/dashboard.component.ts");
/* harmony import */ var _components_user_settings_settings_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/user/settings/settings.component */ "./src/app/components/user/settings/settings.component.ts");
/* harmony import */ var _helpers_auth_guard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./helpers/auth.guard */ "./src/app/helpers/auth.guard.ts");
/* harmony import */ var _components_post_edit_edit_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/post/edit/edit.component */ "./src/app/components/post/edit/edit.component.ts");
/* harmony import */ var _components_post_detail_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/post/detail/detail.component */ "./src/app/components/post/detail/detail.component.ts");
/* harmony import */ var _components_admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/admin/dashboard/dashboard.component */ "./src/app/components/admin/dashboard/dashboard.component.ts");












const routes = [
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"] },
    { path: 'register', component: _components_register_register_component__WEBPACK_IMPORTED_MODULE_5__["RegisterComponent"] },
    { path: 'user/dashboard', component: _components_user_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_6__["UserDashboardComponent"], canActivate: [_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] },
    { path: 'user/settings', component: _components_user_settings_settings_component__WEBPACK_IMPORTED_MODULE_7__["SettingsComponent"], canActivate: [_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] },
    { path: 'admin/dashboard', component: _components_admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_11__["AdminDashboardComponent"], data: { Role: 'Admin' }, canActivate: [_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] },
    { path: 'post/edit', component: _components_post_edit_edit_component__WEBPACK_IMPORTED_MODULE_9__["EditComponent"], canActivate: [_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] },
    { path: 'post/edit/:uid', component: _components_post_edit_edit_component__WEBPACK_IMPORTED_MODULE_9__["EditComponent"], canActivate: [_helpers_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] },
    { path: 'post/detail/:uid', component: _components_post_detail_detail_component__WEBPACK_IMPORTED_MODULE_10__["DetailComponent"] },
    { path: '**', redirectTo: '' }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".sidenav {\n  height: 100vh;\n  width: 300px;\n}\n\nmain {\n  position: relative;\n  min-height: calc(100vh - 64px);\n}\n\n.sidenav__title {\n  margin-left: 1em;\n}\n\n.sidenav_item-icon {\n  margin-right: 0.8em;\n}\n\n.center_icon mat-icon {\n  font-size: 1.5em;\n  margin-right: 0.25em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQzpcXEdJQlogU3R1ZmZcXDAwX1BST0pFS1RBUkJFSVRcXHBvamVrdGFyYmVpdFxcc3JjXFxjbGllbnQvc3JjXFxhcHBcXGFwcC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLFlBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0VBQ0EsOEJBQUE7QUNDSjs7QURFQTtFQUNJLGdCQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtBQ0NKOztBREdJO0VBQ0ksZ0JBQUE7RUFDQSxvQkFBQTtBQ0FSIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNpZGVuYXYge1xuICAgIGhlaWdodDogMTAwdmg7XG4gICAgd2lkdGg6IDMwMHB4O1xufVxuXG5tYWluIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWluLWhlaWdodDogY2FsYygxMDB2aCAtIDY0cHgpO1xufVxuXG4uc2lkZW5hdl9fdGl0bGUge1xuICAgIG1hcmdpbi1sZWZ0OiAxZW07XG59XG5cbi5zaWRlbmF2X2l0ZW0taWNvbiB7XG4gICAgbWFyZ2luLXJpZ2h0OiAwLjhlbTtcbn1cblxuLmNlbnRlcl9pY29uIHtcbiAgICBtYXQtaWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMS41ZW07XG4gICAgICAgIG1hcmdpbi1yaWdodDogMC4yNWVtO1xuICAgIH1cbn0iLCIuc2lkZW5hdiB7XG4gIGhlaWdodDogMTAwdmg7XG4gIHdpZHRoOiAzMDBweDtcbn1cblxubWFpbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWluLWhlaWdodDogY2FsYygxMDB2aCAtIDY0cHgpO1xufVxuXG4uc2lkZW5hdl9fdGl0bGUge1xuICBtYXJnaW4tbGVmdDogMWVtO1xufVxuXG4uc2lkZW5hdl9pdGVtLWljb24ge1xuICBtYXJnaW4tcmlnaHQ6IDAuOGVtO1xufVxuXG4uY2VudGVyX2ljb24gbWF0LWljb24ge1xuICBmb250LXNpemU6IDEuNWVtO1xuICBtYXJnaW4tcmlnaHQ6IDAuMjVlbTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let AppComponent = class AppComponent {
    constructor(router, userService) {
        this.router = router;
        this.userService = userService;
    }
    ngOnInit() {
        this.userService.refreshUserInfo();
    }
    logout() {
        this.userService.logout().subscribe(() => {
            this.router.navigate(['/login']);
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./material.module */ "./src/app/material.module.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_register_register_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/register/register.component */ "./src/app/components/register/register.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _components_user_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/user/dashboard/dashboard.component */ "./src/app/components/user/dashboard/dashboard.component.ts");
/* harmony import */ var _components_user_settings_settings_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/user/settings/settings.component */ "./src/app/components/user/settings/settings.component.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_error_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/error.service */ "./src/app/services/error.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _components_post_detail_detail_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/post/detail/detail.component */ "./src/app/components/post/detail/detail.component.ts");
/* harmony import */ var _components_post_edit_edit_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/post/edit/edit.component */ "./src/app/components/post/edit/edit.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _helpers_auth_guard__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./helpers/auth.guard */ "./src/app/helpers/auth.guard.ts");
/* harmony import */ var _components_shared_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/shared/confirm/confirm.component */ "./src/app/components/shared/confirm/confirm.component.ts");
/* harmony import */ var _components_admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/admin/dashboard/dashboard.component */ "./src/app/components/admin/dashboard/dashboard.component.ts");






















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _components_home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
            _components_login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
            _components_register_register_component__WEBPACK_IMPORTED_MODULE_9__["RegisterComponent"],
            _components_user_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_11__["UserDashboardComponent"],
            _components_admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_21__["AdminDashboardComponent"],
            _components_user_settings_settings_component__WEBPACK_IMPORTED_MODULE_12__["SettingsComponent"],
            _components_post_detail_detail_component__WEBPACK_IMPORTED_MODULE_16__["DetailComponent"],
            _components_post_edit_edit_component__WEBPACK_IMPORTED_MODULE_17__["EditComponent"],
            _components_shared_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_20__["ConfirmComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_18__["HttpClientModule"],
            _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"]
        ],
        providers: [
            _services_auth_service__WEBPACK_IMPORTED_MODULE_13__["AuthService"],
            _services_error_service__WEBPACK_IMPORTED_MODULE_14__["ErrorService"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_15__["UserService"],
            _helpers_auth_guard__WEBPACK_IMPORTED_MODULE_19__["AuthGuard"]
        ],
        entryComponents: [_components_shared_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_20__["ConfirmComponent"]],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/components/admin/dashboard/dashboard.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/admin/dashboard/dashboard.component.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".userPosts {\n  width: 100%;\n}\n.userPosts .mat-row {\n  transition: transform 350ms ease;\n}\n.userPosts .mat-row:hover {\n  cursor: pointer;\n  transform: scale(1.008);\n  transition: transform 250ms ease;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9kYXNoYm9hcmQvQzpcXEdJQlogU3R1ZmZcXDAwX1BST0pFS1RBUkJFSVRcXHBvamVrdGFyYmVpdFxcc3JjXFxjbGllbnQvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGFkbWluXFxkYXNoYm9hcmRcXGRhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQ0NKO0FEQUk7RUFDSSxnQ0FBQTtBQ0VSO0FEQ0k7RUFDSSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQ0FBQTtBQ0NSIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXJQb3N0cyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgLm1hdC1yb3cge1xuICAgICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMzUwbXMgZWFzZTtcbiAgICB9XG4gICAgXG4gICAgLm1hdC1yb3c6aG92ZXIge1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMS4wMDgpO1xuICAgICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgZWFzZTtcbiAgICB9XG59XG4iLCIudXNlclBvc3RzIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4udXNlclBvc3RzIC5tYXQtcm93IHtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDM1MG1zIGVhc2U7XG59XG4udXNlclBvc3RzIC5tYXQtcm93OmhvdmVyIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0cmFuc2Zvcm06IHNjYWxlKDEuMDA4KTtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDI1MG1zIGVhc2U7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/admin/dashboard/dashboard.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/admin/dashboard/dashboard.component.ts ***!
  \*******************************************************************/
/*! exports provided: AdminDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminDashboardComponent", function() { return AdminDashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_post_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/post.service */ "./src/app/services/post.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _shared_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/confirm/confirm.component */ "./src/app/components/shared/confirm/confirm.component.ts");





let AdminDashboardComponent = class AdminDashboardComponent {
    constructor(postService, snackBar, dialog) {
        this.postService = postService;
        this.snackBar = snackBar;
        this.dialog = dialog;
        this.myPosts = new Array();
        this.displayedColumns = ['index', 'title', 'date', 'status', 'edit'];
    }
    ngOnInit() {
        this.getPostsAdmin();
    }
    getPostsAdmin() {
        this.postService.getPostsAdmin().subscribe(posts => {
            this.myPosts = posts;
        }, err => {
            this.snackBar.open('Failed loading the user data!', 'Close', { panelClass: ['error', 'mat-warn'] });
        });
    }
    deletePost(event, uid) {
        event.stopPropagation();
        const dialogRef = this.dialog.open(_shared_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_4__["ConfirmComponent"]);
        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.postService.deletePost(uid).subscribe(posts => {
                    this.getPostsAdmin();
                    this.snackBar.open('Successfully deleted post!', 'Close');
                }, err => {
                    this.snackBar.open('Failed deleting the post!', 'Close', { panelClass: ['error', 'mat-warn'] });
                });
            }
        });
    }
    toggleStatus(event, uid) {
        event.stopPropagation();
        const dialogRef = this.dialog.open(_shared_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_4__["ConfirmComponent"]);
        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.postService.toggleStatus(uid).subscribe(posts => {
                    this.getPostsAdmin();
                    this.snackBar.open('Successfully successfully published or hidden!', 'Close');
                }, err => {
                    this.snackBar.open('Failed!', 'Close', { panelClass: ['error', 'mat-warn'] });
                });
            }
        });
    }
    convertToString(str) {
        return new Date(str).toLocaleString('de-Ch');
    }
};
AdminDashboardComponent.ctorParameters = () => [
    { type: src_app_services_post_service__WEBPACK_IMPORTED_MODULE_2__["PostService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] }
];
AdminDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/admin/dashboard/dashboard.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/components/admin/dashboard/dashboard.component.scss")).default]
    })
], AdminDashboardComponent);



/***/ }),

/***/ "./src/app/components/home/home.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrapper {\n  margin: 2rem 1rem;\n}\n.wrapper .actions {\n  display: none;\n}\n.post {\n  margin-bottom: 2rem;\n  cursor: pointer;\n}\n.post .avatar-image {\n  background-image: url(\"https://material.angular.io/assets/img/examples/shiba1.jpg\");\n  background-size: cover;\n}\n.post mat-card-actions {\n  display: flex;\n  align-items: center;\n}\n.post mat-card-content p {\n  margin: 0 16px;\n}\n.post mat-icon {\n  font-size: 1.5em;\n  margin-right: 0.25em;\n}\n@media (min-width: 768px) {\n  .wrapper {\n    display: grid;\n    margin-top: 2rem;\n    grid-template-columns: auto minmax(280px, 640px) 300px auto;\n    -moz-column-gap: 2rem;\n         column-gap: 2rem;\n  }\n  .wrapper .posts {\n    grid-column: 2/3;\n  }\n  .wrapper .actions {\n    display: block;\n    grid-column: 3/4;\n  }\n  .wrapper .actions .top-posts {\n    margin-bottom: 2rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ob21lL0M6XFxHSUJaIFN0dWZmXFwwMF9QUk9KRUtUQVJCRUlUXFxwb2pla3RhcmJlaXRcXHNyY1xcY2xpZW50L3NyY1xcYXBwXFxjb21wb25lbnRzXFxob21lXFxob21lLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0FDQ0o7QURBSTtFQUNJLGFBQUE7QUNFUjtBREVBO0VBQ0ksbUJBQUE7RUFDQSxlQUFBO0FDQ0o7QURBSTtFQUNJLG1GQUFBO0VBQ0Esc0JBQUE7QUNFUjtBRENJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FDQ1I7QURFSTtFQUNJLGNBQUE7QUNBUjtBREdJO0VBQ0ksZ0JBQUE7RUFDQSxvQkFBQTtBQ0RSO0FES0E7RUFDSTtJQUNJLGFBQUE7SUFDQSxnQkFBQTtJQUNBLDJEQUFBO0lBQ0EscUJBQUE7U0FBQSxnQkFBQTtFQ0ZOO0VER007SUFDSSxnQkFBQTtFQ0RWO0VER007SUFDSSxjQUFBO0lBQ0EsZ0JBQUE7RUNEVjtFREVVO0lBQ0ksbUJBQUE7RUNBZDtBQUNGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud3JhcHBlciB7XG4gICAgbWFyZ2luOiAycmVtIDFyZW07XG4gICAgLmFjdGlvbnMge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbn1cblxuLnBvc3Qge1xuICAgIG1hcmdpbi1ib3R0b206IDJyZW07XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIC5hdmF0YXItaW1hZ2Uge1xuICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ2h0dHBzOi8vbWF0ZXJpYWwuYW5ndWxhci5pby9hc3NldHMvaW1nL2V4YW1wbGVzL3NoaWJhMS5qcGcnKTtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICB9XG5cbiAgICBtYXQtY2FyZC1hY3Rpb25zIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB9XG5cbiAgICBtYXQtY2FyZC1jb250ZW50IHAge1xuICAgICAgICBtYXJnaW46IDAgMTZweDtcbiAgICB9XG5cbiAgICBtYXQtaWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMS41ZW07XG4gICAgICAgIG1hcmdpbi1yaWdodDogMC4yNWVtO1xuICAgIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gICAgLndyYXBwZXIge1xuICAgICAgICBkaXNwbGF5OiBncmlkO1xuICAgICAgICBtYXJnaW4tdG9wOiAycmVtOztcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBhdXRvIG1pbm1heCgyODBweCwgNjQwcHgpIDMwMHB4IGF1dG87XG4gICAgICAgIGNvbHVtbi1nYXA6IDJyZW07XG4gICAgICAgIC5wb3N0cyB7XG4gICAgICAgICAgICBncmlkLWNvbHVtbjogMi8zO1xuICAgICAgICB9XG4gICAgICAgIC5hY3Rpb25zIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgZ3JpZC1jb2x1bW46IDMvNDtcbiAgICAgICAgICAgIC50b3AtcG9zdHMge1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9ICAgIFxufVxuIiwiLndyYXBwZXIge1xuICBtYXJnaW46IDJyZW0gMXJlbTtcbn1cbi53cmFwcGVyIC5hY3Rpb25zIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLnBvc3Qge1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4ucG9zdCAuYXZhdGFyLWltYWdlIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiaHR0cHM6Ly9tYXRlcmlhbC5hbmd1bGFyLmlvL2Fzc2V0cy9pbWcvZXhhbXBsZXMvc2hpYmExLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cbi5wb3N0IG1hdC1jYXJkLWFjdGlvbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnBvc3QgbWF0LWNhcmQtY29udGVudCBwIHtcbiAgbWFyZ2luOiAwIDE2cHg7XG59XG4ucG9zdCBtYXQtaWNvbiB7XG4gIGZvbnQtc2l6ZTogMS41ZW07XG4gIG1hcmdpbi1yaWdodDogMC4yNWVtO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLndyYXBwZXIge1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgbWFyZ2luLXRvcDogMnJlbTtcbiAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IGF1dG8gbWlubWF4KDI4MHB4LCA2NDBweCkgMzAwcHggYXV0bztcbiAgICBjb2x1bW4tZ2FwOiAycmVtO1xuICB9XG4gIC53cmFwcGVyIC5wb3N0cyB7XG4gICAgZ3JpZC1jb2x1bW46IDIvMztcbiAgfVxuICAud3JhcHBlciAuYWN0aW9ucyB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgZ3JpZC1jb2x1bW46IDMvNDtcbiAgfVxuICAud3JhcHBlciAuYWN0aW9ucyAudG9wLXBvc3RzIHtcbiAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xuICB9XG59Il19 */");

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var src_app_services_post_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/post.service */ "./src/app/services/post.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");





let HomeComponent = class HomeComponent {
    constructor(userService, postService, snackBar) {
        this.userService = userService;
        this.postService = postService;
        this.snackBar = snackBar;
    }
    ngOnInit() {
        this.getAllPosts();
    }
    getAllPosts() {
        this.postService.getPosts().subscribe(posts => {
            this.allPosts = posts;
        }, err => {
            this.snackBar.open('Failed fetching the posst!', 'Close', { panelClass: ['error', 'mat-warn'] });
        });
    }
};
HomeComponent.ctorParameters = () => [
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"] },
    { type: src_app_services_post_service__WEBPACK_IMPORTED_MODULE_3__["PostService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"] }
];
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/home/home.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.component.scss */ "./src/app/components/home/home.component.scss")).default]
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/components/login/login.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  margin: 2rem auto;\n  max-width: 500px;\n}\n\n.buttons {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end;\n}\n\n.buttons button {\n  margin-left: 1rem;\n}\n\nmat-card {\n  padding: 2rem;\n}\n\nmat-form-field {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9DOlxcR0lCWiBTdHVmZlxcMDBfUFJPSkVLVEFSQkVJVFxccG9qZWt0YXJiZWl0XFxzcmNcXGNsaWVudC9zcmNcXGFwcFxcY29tcG9uZW50c1xcbG9naW5cXGxvZ2luLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QUNDSjs7QURDSTtFQUNJLGlCQUFBO0FDQ1I7O0FER0E7RUFDSSxhQUFBO0FDQUo7O0FER0E7RUFDSSxXQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XG4gICAgbWFyZ2luOiAycmVtIGF1dG87XG4gICAgbWF4LXdpZHRoOiA1MDBweDtcbn1cblxuLmJ1dHRvbnMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuXG4gICAgYnV0dG9uIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gICAgfVxufVxuXG5tYXQtY2FyZCB7XG4gICAgcGFkZGluZzogMnJlbTtcbn1cblxubWF0LWZvcm0tZmllbGQge1xuICAgIHdpZHRoOiAxMDAlO1xufSIsIi5jb250YWluZXIge1xuICBtYXJnaW46IDJyZW0gYXV0bztcbiAgbWF4LXdpZHRoOiA1MDBweDtcbn1cblxuLmJ1dHRvbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuLmJ1dHRvbnMgYnV0dG9uIHtcbiAgbWFyZ2luLWxlZnQ6IDFyZW07XG59XG5cbm1hdC1jYXJkIHtcbiAgcGFkZGluZzogMnJlbTtcbn1cblxubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");






let LoginComponent = class LoginComponent {
    constructor(router, userService, snackBar) {
        this.router = router;
        this.userService = userService;
        this.snackBar = snackBar;
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])
        });
        this.tokenForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            token: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])
        });
        this.tfaActive = false;
    }
    submit() {
        if ((this.form.valid && !this.tfaActive) || (this.tokenForm.valid && this.tfaActive)) {
            this.userService.login(this.form.controls.username.value, this.form.controls.password.value, this.tfaActive ? this.tokenForm.controls.token.value : null)
                .subscribe(data => {
                if (data.status === 200) {
                    this.userService.currentUser.subscribe(user => {
                        if (user && user.Role === 'Admin') {
                            this.router.navigate(['/admin/dashboard']);
                        }
                        else {
                            this.router.navigate(['/user/dashboard']);
                        }
                    });
                }
                if (data.status === 206) {
                    this.tfaActive = true;
                }
            }, err => {
                this.snackBar.open('Login failed!', 'Close', { panelClass: ['error', 'mat-warn'] });
            });
        }
    }
    hasError(controlName, errorName) {
        return this.form.controls[controlName].hasError(errorName);
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/login/login.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.scss */ "./src/app/components/login/login.component.scss")).default]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/components/post/detail/detail.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/components/post/detail/detail.component.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  padding-top: 2rem;\n  max-width: 640px;\n}\n\nmat-card {\n  margin: 2rem auto 0 auto;\n}\n\nmat-card mat-card-content p {\n  margin: 0 16px;\n}\n\nmat-form-field {\n  width: 100%;\n}\n\ntextarea {\n  min-height: 60px;\n}\n\n.comments {\n  margin-top: 2rem;\n}\n\n.margin {\n  margin: 0 16px;\n}\n\n.comment {\n  padding: 0.5rem 1rem;\n  margin-bottom: 1rem;\n  background-color: #eee;\n  border-radius: 3px;\n}\n\n.comment h3 {\n  margin: 0 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wb3N0L2RldGFpbC9DOlxcR0lCWiBTdHVmZlxcMDBfUFJPSkVLVEFSQkVJVFxccG9qZWt0YXJiZWl0XFxzcmNcXGNsaWVudC9zcmNcXGFwcFxcY29tcG9uZW50c1xccG9zdFxcZGV0YWlsXFxkZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvcG9zdC9kZXRhaWwvZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtBQ0NKOztBRENBO0VBQ0ksd0JBQUE7QUNFSjs7QURESTtFQUNJLGNBQUE7QUNHUjs7QURDQTtFQUNJLFdBQUE7QUNFSjs7QURDQTtFQUNJLGdCQUFBO0FDRUo7O0FEQ0E7RUFDSSxnQkFBQTtBQ0VKOztBRENBO0VBQ0ksY0FBQTtBQ0VKOztBRENBO0VBQ0ksb0JBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUNFSjs7QURESTtFQUNJLGNBQUE7QUNHUiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcG9zdC9kZXRhaWwvZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XG4gICAgcGFkZGluZy10b3A6IDJyZW07XG4gICAgbWF4LXdpZHRoOiA2NDBweDtcbn1cbm1hdC1jYXJkIHtcbiAgICBtYXJnaW46IDJyZW0gYXV0byAwIGF1dG87XG4gICAgbWF0LWNhcmQtY29udGVudCBwIHtcbiAgICAgICAgbWFyZ2luOiAwIDE2cHg7XG4gICAgfVxufVxuXG5tYXQtZm9ybS1maWVsZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbnRleHRhcmVhIHtcbiAgICBtaW4taGVpZ2h0OiA2MHB4O1xufVxuXG4uY29tbWVudHMge1xuICAgIG1hcmdpbi10b3A6IDJyZW07XG59XG5cbi5tYXJnaW4ge1xuICAgIG1hcmdpbjogMCAxNnB4O1xufVxuXG4uY29tbWVudCB7XG4gICAgcGFkZGluZzogMC41cmVtIDFyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBoMyB7XG4gICAgICAgIG1hcmdpbjogMCAxNnB4O1xuICAgIH1cbn0iLCIuY29udGFpbmVyIHtcbiAgcGFkZGluZy10b3A6IDJyZW07XG4gIG1heC13aWR0aDogNjQwcHg7XG59XG5cbm1hdC1jYXJkIHtcbiAgbWFyZ2luOiAycmVtIGF1dG8gMCBhdXRvO1xufVxubWF0LWNhcmQgbWF0LWNhcmQtY29udGVudCBwIHtcbiAgbWFyZ2luOiAwIDE2cHg7XG59XG5cbm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbnRleHRhcmVhIHtcbiAgbWluLWhlaWdodDogNjBweDtcbn1cblxuLmNvbW1lbnRzIHtcbiAgbWFyZ2luLXRvcDogMnJlbTtcbn1cblxuLm1hcmdpbiB7XG4gIG1hcmdpbjogMCAxNnB4O1xufVxuXG4uY29tbWVudCB7XG4gIHBhZGRpbmc6IDAuNXJlbSAxcmVtO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG59XG4uY29tbWVudCBoMyB7XG4gIG1hcmdpbjogMCAxNnB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/components/post/detail/detail.component.ts":
/*!************************************************************!*\
  !*** ./src/app/components/post/detail/detail.component.ts ***!
  \************************************************************/
/*! exports provided: DetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailComponent", function() { return DetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var src_app_services_post_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/post.service */ "./src/app/services/post.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");








let DetailComponent = class DetailComponent {
    constructor(location, postService, route, snackBar, userService) {
        this.location = location;
        this.postService = postService;
        this.route = route;
        this.snackBar = snackBar;
        this.userService = userService;
        this.commentForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormGroup"]({
            comment: new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(200)])
        });
    }
    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['uid']) {
                this.uid = params['uid'];
                this.getPostDetail();
            }
        });
    }
    getPostDetail() {
        this.postService.getPostDetail(this.uid).subscribe(post => {
            this.post = post;
        }, err => {
            this.snackBar.open('Post creation failed', 'Close', { panelClass: ['error', 'mat-warn'] });
        });
    }
    postComment() {
        if (this.commentForm.valid) {
            this.postService.postComment(this.uid, this.commentForm.controls.comment.value).subscribe(() => {
                this.commentForm.controls.comment.setValue('');
                this.getPostDetail();
            }, err => {
                this.snackBar.open('Post comment failed', 'Close', { panelClass: ['error', 'mat-warn'] });
            });
        }
    }
    convertToString(str) {
        return new Date(str).toLocaleString('de-Ch');
    }
};
DetailComponent.ctorParameters = () => [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"] },
    { type: src_app_services_post_service__WEBPACK_IMPORTED_MODULE_3__["PostService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"] },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] }
];
DetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/post/detail/detail.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./detail.component.scss */ "./src/app/components/post/detail/detail.component.scss")).default]
    })
], DetailComponent);



/***/ }),

/***/ "./src/app/components/post/edit/edit.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/components/post/edit/edit.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  padding-top: 2rem;\n  max-width: 640px;\n}\n\nmat-card {\n  margin: 2rem auto 0 auto;\n}\n\ntextarea {\n  min-height: 200px;\n}\n\nmat-form-field {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wb3N0L2VkaXQvQzpcXEdJQlogU3R1ZmZcXDAwX1BST0pFS1RBUkJFSVRcXHBvamVrdGFyYmVpdFxcc3JjXFxjbGllbnQvc3JjXFxhcHBcXGNvbXBvbmVudHNcXHBvc3RcXGVkaXRcXGVkaXQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvcG9zdC9lZGl0L2VkaXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FEQ0E7RUFDSSx3QkFBQTtBQ0VKOztBRENBO0VBQ0ksaUJBQUE7QUNFSjs7QURDQTtFQUNJLFdBQUE7QUNFSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcG9zdC9lZGl0L2VkaXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyIHtcbiAgICBwYWRkaW5nLXRvcDogMnJlbTtcbiAgICBtYXgtd2lkdGg6IDY0MHB4O1xufVxubWF0LWNhcmQge1xuICAgIG1hcmdpbjogMnJlbSBhdXRvIDAgYXV0bztcbn1cblxudGV4dGFyZWEge1xuICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xufVxuXG5tYXQtZm9ybS1maWVsZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59IiwiLmNvbnRhaW5lciB7XG4gIHBhZGRpbmctdG9wOiAycmVtO1xuICBtYXgtd2lkdGg6IDY0MHB4O1xufVxuXG5tYXQtY2FyZCB7XG4gIG1hcmdpbjogMnJlbSBhdXRvIDAgYXV0bztcbn1cblxudGV4dGFyZWEge1xuICBtaW4taGVpZ2h0OiAyMDBweDtcbn1cblxubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/components/post/edit/edit.component.ts":
/*!********************************************************!*\
  !*** ./src/app/components/post/edit/edit.component.ts ***!
  \********************************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_models_post_viewmodel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/models/post.viewmodel */ "./src/app/models/post.viewmodel.ts");
/* harmony import */ var src_app_services_post_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/post.service */ "./src/app/services/post.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");








let EditComponent = class EditComponent {
    constructor(route, router, postService, snackBar, location) {
        this.route = route;
        this.router = router;
        this.postService = postService;
        this.snackBar = snackBar;
        this.location = location;
        this.isEditMode = false;
    }
    ngOnInit() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            Title: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            Content: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])
        });
        this.route.params.subscribe(params => {
            if (params['uid']) {
                // edit
                this.uid = params['uid'];
                this.getExistingPost(params['uid']);
                this.isEditMode = true;
            }
            else {
                // create
                this.form.patchValue(new src_app_models_post_viewmodel__WEBPACK_IMPORTED_MODULE_4__["Post"]());
            }
        });
    }
    getExistingPost(uid) {
        this.postService.getPost(uid).subscribe(post => {
            this.form.patchValue(post);
        }, err => {
            this.snackBar.open('Post loading', 'Close', { panelClass: ['error', 'mat-warn'] });
        });
    }
    createPost() {
        if (this.form.valid) {
            this.postService.createPost(this.form.getRawValue()).subscribe(() => {
                this.router.navigate(['/user/dashboard']);
                this.snackBar.open('Post creation successful', 'Close');
            }, err => {
                this.snackBar.open('Post creation failed', 'Close', { panelClass: ['error', 'mat-warn'] });
            });
        }
    }
    savePost() {
        if (this.form.valid) {
            this.postService.updatePost(this.uid, this.form.getRawValue()).subscribe(() => {
                this.getExistingPost(this.uid);
                this.snackBar.open('Post successfully saved!', 'Close');
            }, err => {
                this.snackBar.open('Post saving failed', 'Close', { panelClass: ['error', 'mat-warn'] });
            });
        }
    }
};
EditComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_services_post_service__WEBPACK_IMPORTED_MODULE_5__["PostService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSnackBar"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"] }
];
EditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./edit.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/post/edit/edit.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./edit.component.scss */ "./src/app/components/post/edit/edit.component.scss")).default]
    })
], EditComponent);



/***/ }),

/***/ "./src/app/components/register/register.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/register/register.component.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  margin: 2rem auto;\n  max-width: 500px;\n}\n\n.buttons {\n  display: flex;\n  align-items: center;\n  justify-content: flex-end;\n}\n\n.buttons button {\n  margin-left: 1rem;\n}\n\nmat-card {\n  padding: 2rem;\n}\n\nmat-form-field {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZWdpc3Rlci9DOlxcR0lCWiBTdHVmZlxcMDBfUFJPSkVLVEFSQkVJVFxccG9qZWt0YXJiZWl0XFxzcmNcXGNsaWVudC9zcmNcXGFwcFxcY29tcG9uZW50c1xccmVnaXN0ZXJcXHJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QUNDSjs7QURDSTtFQUNJLGlCQUFBO0FDQ1I7O0FER0E7RUFDSSxhQUFBO0FDQUo7O0FER0E7RUFDSSxXQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XG4gICAgbWFyZ2luOiAycmVtIGF1dG87XG4gICAgbWF4LXdpZHRoOiA1MDBweDtcbn1cblxuLmJ1dHRvbnMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuXG4gICAgYnV0dG9uIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gICAgfVxufVxuXG5tYXQtY2FyZCB7XG4gICAgcGFkZGluZzogMnJlbTtcbn1cblxubWF0LWZvcm0tZmllbGQge1xuICAgIHdpZHRoOiAxMDAlO1xufSIsIi5jb250YWluZXIge1xuICBtYXJnaW46IDJyZW0gYXV0bztcbiAgbWF4LXdpZHRoOiA1MDBweDtcbn1cblxuLmJ1dHRvbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuLmJ1dHRvbnMgYnV0dG9uIHtcbiAgbWFyZ2luLWxlZnQ6IDFyZW07XG59XG5cbm1hdC1jYXJkIHtcbiAgcGFkZGluZzogMnJlbTtcbn1cblxubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/register/register.component.ts ***!
  \***********************************************************/
/*! exports provided: RegisterComponent, ShowAfterTouchedErrorStateMatcher, ShowImmediateErrorStateMatcher */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowAfterTouchedErrorStateMatcher", function() { return ShowAfterTouchedErrorStateMatcher; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowImmediateErrorStateMatcher", function() { return ShowImmediateErrorStateMatcher; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");







let RegisterComponent = class RegisterComponent {
    constructor(router, userService, snackBar) {
        this.router = router;
        this.userService = userService;
        this.snackBar = snackBar;
        this.showAfterTouched = new ShowAfterTouchedErrorStateMatcher();
        this.showImmediate = new ShowImmediateErrorStateMatcher();
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(50)]),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(20)], this.usernameAlreadyTaken.bind(this)),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            confirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
        }, [this.checkPasswords]);
    }
    ngOnInit() { }
    submit() {
        if (this.form.valid) {
            this.userService.register(this.form.controls.name.value, this.form.controls.username.value, this.form.controls.password.value)
                .subscribe(data => {
                if (data.status === 200) {
                    this.router.navigate(['/login']);
                }
                if (data.status === 206) {
                    // this.tfaFlag = true;
                }
            }, err => {
                this.snackBar.open('Register failed!', 'Close', { panelClass: ['error', 'mat-warn'] });
            });
        }
    }
    hasError(controlName, errorName) {
        return this.form.controls[controlName].hasError(errorName);
    }
    checkPasswords(group) {
        const pass = group.get('password').value;
        const confirmPass = group.get('confirmPassword').value;
        return pass === confirmPass ? null : { notSame: true };
    }
    usernameAlreadyTaken(control) {
        return this.userService.isUsernameTaken(control.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(res => {
            return res ? { usernameTaken: true } : null;
        }));
    }
};
RegisterComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"] }
];
RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/register/register.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./register.component.scss */ "./src/app/components/register/register.component.scss")).default]
    })
], RegisterComponent);

class ShowAfterTouchedErrorStateMatcher {
    isErrorState(control, form) {
        const controlBeenTouched = !!(control && (control.touched));
        const parentHasNotSameError = !!(control && control.parent && control.parent.hasError('notSame'));
        return controlBeenTouched && parentHasNotSameError;
    }
}
class ShowImmediateErrorStateMatcher {
    isErrorState(control, form) {
        const controlBeenTouched = !!(control && control.invalid && (control.dirty || control.touched));
        return controlBeenTouched;
    }
}


/***/ }),

/***/ "./src/app/components/shared/confirm/confirm.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/components/shared/confirm/confirm.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2hhcmVkL2NvbmZpcm0vY29uZmlybS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/components/shared/confirm/confirm.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/shared/confirm/confirm.component.ts ***!
  \****************************************************************/
/*! exports provided: ConfirmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmComponent", function() { return ConfirmComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



let ConfirmComponent = class ConfirmComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    onNoClick() {
        this.dialogRef.close();
    }
};
ConfirmComponent.ctorParameters = () => [
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
];
ConfirmComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-confirm',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./confirm.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shared/confirm/confirm.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./confirm.component.scss */ "./src/app/components/shared/confirm/confirm.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"]))
], ConfirmComponent);



/***/ }),

/***/ "./src/app/components/user/dashboard/dashboard.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/components/user/dashboard/dashboard.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlci9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/components/user/dashboard/dashboard.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/components/user/dashboard/dashboard.component.ts ***!
  \******************************************************************/
/*! exports provided: UserDashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDashboardComponent", function() { return UserDashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_post_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/post.service */ "./src/app/services/post.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _shared_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/confirm/confirm.component */ "./src/app/components/shared/confirm/confirm.component.ts");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");






let UserDashboardComponent = class UserDashboardComponent {
    constructor(postService, snackBar, dialog, userService) {
        this.postService = postService;
        this.snackBar = snackBar;
        this.dialog = dialog;
        this.userService = userService;
        this.myPosts = new Array();
        this.displayedColumns = ['index', 'title', 'date', 'status', 'edit'];
    }
    ngOnInit() {
        this.getMyPosts();
    }
    getMyPosts() {
        this.postService.getMyPosts().subscribe(posts => {
            this.myPosts = posts;
        }, err => {
            this.snackBar.open('Failed loading the user data!', 'Close', { panelClass: ['error', 'mat-warn'] });
        });
    }
    deletePost(event, uid) {
        event.stopPropagation();
        const dialogRef = this.dialog.open(_shared_confirm_confirm_component__WEBPACK_IMPORTED_MODULE_4__["ConfirmComponent"]);
        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.postService.deletePost(uid).subscribe(posts => {
                    this.getMyPosts();
                    this.snackBar.open('Successfully deleted post!', 'Close');
                }, err => {
                    this.snackBar.open('Failed deleting the post!', 'Close', { panelClass: ['error', 'mat-warn'] });
                });
            }
        });
    }
    convertToString(str) {
        return new Date(str).toLocaleString('de-Ch');
    }
};
UserDashboardComponent.ctorParameters = () => [
    { type: src_app_services_post_service__WEBPACK_IMPORTED_MODULE_2__["PostService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] },
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] }
];
UserDashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/dashboard/dashboard.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/components/user/dashboard/dashboard.component.scss")).default]
    })
], UserDashboardComponent);



/***/ }),

/***/ "./src/app/components/user/settings/settings.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/components/user/settings/settings.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("mat-form-field {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy91c2VyL3NldHRpbmdzL0M6XFxHSUJaIFN0dWZmXFwwMF9QUk9KRUtUQVJCRUlUXFxwb2pla3RhcmJlaXRcXHNyY1xcY2xpZW50L3NyY1xcYXBwXFxjb21wb25lbnRzXFx1c2VyXFxzZXR0aW5nc1xcc2V0dGluZ3MuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvdXNlci9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlci9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1mb3JtLWZpZWxkIHtcbiAgICB3aWR0aDogMTAwJTtcbn0iLCJtYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/components/user/settings/settings.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/user/settings/settings.component.ts ***!
  \****************************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");






let SettingsComponent = class SettingsComponent {
    constructor(userService, authService, snackBar) {
        this.userService = userService;
        this.authService = authService;
        this.snackBar = snackBar;
        this.phoneForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])
        });
        this.tokenForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            token: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])
        });
        this.tfaActive = false;
    }
    ngOnInit() {
        this.getUserData();
    }
    getUserData() {
        this.userService.getUserData().subscribe(userInfo => {
            this.userInfo = userInfo;
            this.phoneForm.patchValue({ phone: this.userInfo.Phone });
        }, err => {
            this.snackBar.open('Couldnt load the user data!', 'Close', { panelClass: ['error', 'mat-warn'] });
        });
    }
    submitPhoneForm() {
        if (this.phoneForm.valid) {
            this.authService.setupTFA(this.phoneForm.controls.phone.value).subscribe(res => {
                if (res.status === 200) {
                    this.tfaActive = true;
                    this.getUserData();
                    this.snackBar.open('Successfully sent token! Please check your phone and verify it!', 'Close');
                }
                else {
                    this.snackBar.open('Failed sending the token! Please check your phone number.', 'Close', { panelClass: ['error', 'mat-warn'] });
                }
            });
        }
    }
    saveUserData() {
        this.userService.saveUserData(this.userInfo).subscribe(() => {
            this.getUserData();
            this.snackBar.open('Data successfully saved!', 'Close');
        }, err => {
            this.snackBar.open('Failed saving the user data!', 'Close', { panelClass: ['error', 'mat-warn'] });
        });
    }
    disableTFA() {
        this.authService.disableTFA().subscribe(res => {
            this.tfaActive = false;
            this.getUserData();
            this.snackBar.open('Successfully disabled tfa!', 'Close');
        }, err => {
            this.snackBar.open('Failed disabling tfa!', 'Close', { panelClass: ['error', 'mat-warn'] });
        });
    }
    submitTokenForm() {
        if (this.tokenForm.valid) {
            this.authService.verifyToken(this.tokenForm.controls.token.value).subscribe(res => {
                if (res.status === 200) {
                    this.tfaActive = false;
                    this.getUserData();
                    this.snackBar.open('Successfully enabled tfa!', 'Close');
                }
                else {
                    this.snackBar.open('Failed enabling tfa! Please enter the correct token.', 'Close', { panelClass: ['error', 'mat-warn'] });
                }
            });
        }
    }
};
SettingsComponent.ctorParameters = () => [
    { type: src_app_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"] },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"] }
];
SettingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-settings',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./settings.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/user/settings/settings.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./settings.component.scss */ "./src/app/components/user/settings/settings.component.scss")).default]
    })
], SettingsComponent);



/***/ }),

/***/ "./src/app/helpers/auth.guard.ts":
/*!***************************************!*\
  !*** ./src/app/helpers/auth.guard.ts ***!
  \***************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");






let AuthGuard = class AuthGuard {
    constructor(userService, router, snackBar) {
        this.userService = userService;
        this.router = router;
        this.snackBar = snackBar;
    }
    canActivate(nextRoute, state) {
        const canActiveFunction = (currentUser) => {
            if (nextRoute.data['Role']) {
                if (currentUser && currentUser['Role'] === nextRoute.data['Role']) {
                    return true;
                }
                else {
                    this.router.navigate(['/']);
                    this.snackBar.open('You aren\'t allowed to view this page!', 'Close', { panelClass: ['error', 'mat-warn'] });
                    return false;
                }
            }
            else {
                if (this.userService.isLoggedIn() && !nextRoute.data['Role']) {
                    return true;
                }
                else {
                    this.router.navigate(['/login']);
                    return false;
                }
            }
        };
        // If value is empty, first try reloading it and then check again
        if (!this.userService.currentUserSubject.value) {
            return this.userService.getUserData().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(currentUser => {
                return canActiveFunction(currentUser);
            }));
        }
        else {
            return canActiveFunction(this.userService.currentUserSubject.value);
        }
    }
};
AuthGuard.ctorParameters = () => [
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"] }
];
AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], AuthGuard);



/***/ }),

/***/ "./src/app/material.module.ts":
/*!************************************!*\
  !*** ./src/app/material.module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");



let MaterialModule = class MaterialModule {
};
MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        providers: [
            { provide: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_SNACK_BAR_DEFAULT_OPTIONS"], useValue: { duration: 2500 } }
        ],
        imports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"]
        ],
        exports: [
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"]
        ]
    })
], MaterialModule);



/***/ }),

/***/ "./src/app/models/post.viewmodel.ts":
/*!******************************************!*\
  !*** ./src/app/models/post.viewmodel.ts ***!
  \******************************************/
/*! exports provided: Post */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Post", function() { return Post; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Post {
}


/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _error_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./error.service */ "./src/app/services/error.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let AuthService = class AuthService {
    constructor(http, errorService) {
        this.http = http;
        this.errorService = errorService;
        this.baseUrlAuth = 'api/auth';
        this.baseUrlTfa = 'api/tfa';
    }
    register(name, username, password) {
        return this.http.post(`${this.baseUrlAuth}/register`, { name, username, password }, { observe: 'response' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    login(username, password, token) {
        let headerOptions = null;
        if (token) {
            headerOptions = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'x-tfa': token
            });
        }
        return this.http.post(`${this.baseUrlAuth}/login`, { username, password }, { observe: 'response', headers: headerOptions })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    /*---- TFA START ----*/
    setupTFA(phone) {
        return this.http.post(`${this.baseUrlTfa}/setup`, { phone }, { observe: 'response' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    verifyAndUpdateToken() {
        return this.http.get(`${this.baseUrlAuth}/verifyAndUpdateToken`, { observe: 'response' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    verifyToken(token) {
        return this.http.post(`${this.baseUrlTfa}/setup/token`, { token }, { observe: 'response' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    disableTFA() {
        return this.http.delete(`${this.baseUrlTfa}`, { observe: 'response' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _error_service__WEBPACK_IMPORTED_MODULE_3__["ErrorService"] }
];
AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], AuthService);



/***/ }),

/***/ "./src/app/services/error.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/error.service.ts ***!
  \*******************************************/
/*! exports provided: ErrorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorService", function() { return ErrorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");


class ErrorService {
    handleError(error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.log('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.log(`Backend returned code ${error.status}, ` +
                `body was: ${JSON.stringify(error.error)}`);
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["throwError"])(error.error.message ? error.error.message : 'Something bad happened; please try again later.');
    }
    getClientMessage(error) {
        if (!navigator.onLine) {
            return 'No Internet Connection';
        }
        return error.message ? error.message : error.toString();
    }
    getClientStack(error) {
        return error.stack;
    }
    getServerMessage(error) {
        return error.error && error.error.message ? error.error.message : error.message;
    }
    getServerStack(error) {
        // handle stack trace
        return 'stack';
    }
}


/***/ }),

/***/ "./src/app/services/post.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/post.service.ts ***!
  \******************************************/
/*! exports provided: PostService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostService", function() { return PostService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _error_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./error.service */ "./src/app/services/error.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let PostService = class PostService {
    constructor(http, errorService) {
        this.http = http;
        this.errorService = errorService;
        this.baseUrl = 'api/post';
    }
    getPosts() {
        return this.http.get(`${this.baseUrl}`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    getMyPosts() {
        return this.http.get(`${this.baseUrl}/my`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    createPost(post) {
        return this.http.post(`${this.baseUrl}`, { post })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    updatePost(uid, post) {
        return this.http.put(`${this.baseUrl}/${uid}`, { post })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    getPost(uid) {
        return this.http.get(`${this.baseUrl}/${uid}`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    getPostDetail(uid) {
        return this.http.get(`${this.baseUrl}/${uid}/detail`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    /*----Admin START ----*/
    getPostsAdmin() {
        return this.http.get(`${this.baseUrl}/admin`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    deletePost(uid) {
        return this.http.delete(`${this.baseUrl}/${uid}`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    postComment(postUid, comment) {
        return this.http.post(`${this.baseUrl}/comment`, { postUid, comment })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
    toggleStatus(uid) {
        return this.http.put(`${this.baseUrl}/${uid}/toggleStatus`, { uid })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorService.handleError));
    }
};
PostService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _error_service__WEBPACK_IMPORTED_MODULE_3__["ErrorService"] }
];
PostService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], PostService);



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _error_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./error.service */ "./src/app/services/error.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");








let UserService = class UserService {
    constructor(authService, http, errorService, router) {
        this.authService = authService;
        this.http = http;
        this.errorService = errorService;
        this.router = router;
        this.baseUrl = 'api/auth';
        this.currentUserSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }
    isLoggedIn() {
        return localStorage.getItem('Expiry') && parseInt(localStorage.getItem('Expiry'), 10) > new Date().getTime();
    }
    login(username, password, token) {
        return this.authService.login(username, password, token)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(data => {
            if (data && data.status === 200 && data.body['Expiry']) {
                localStorage.setItem('Expiry', data.body['Expiry']);
                this.currentUserSubject.next(data.body['user']);
            }
            return data;
        }));
    }
    isUsernameTaken(username) {
        return this.http.get(`${this.baseUrl}/isUsernameTaken?u=${username}`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.errorService.handleError));
    }
    isAdmin() {
        if (this.isLoggedIn()) {
            return this.currentUser
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(currentUser => {
                return currentUser && currentUser.Role === 'Admin';
            }));
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(false);
    }
    register(name, username, password) {
        return this.authService.register(name, username, password);
    }
    getUserData() {
        if (this.isLoggedIn()) {
            return this.http.get(`${this.baseUrl}/me`)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.errorService.handleError));
        }
        return null;
    }
    saveUserData(user) {
        return this.http.post(`${this.baseUrl}/me`, { user })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.errorService.handleError));
    }
    // Logout the current user
    logout() {
        return this.http.post(`${this.baseUrl}/logout`, {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(() => {
            this.currentUserSubject.next(null);
            localStorage.removeItem('Expiry');
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.errorService.handleError));
    }
    // Create new userSubject if the ExpiryDate of 1h is exceeded
    refreshUserInfo() {
        this.currentUser.subscribe(currentUser => {
            if (!currentUser && this.isLoggedIn()) {
                this.getUserData().subscribe(newUserObject => {
                    console.log('User reloaded');
                    this.currentUserSubject.next(newUserObject);
                });
            }
        });
    }
};
UserService.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"] },
    { type: _error_service__WEBPACK_IMPORTED_MODULE_6__["ErrorService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] }
];
UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], UserService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");






if (_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_4__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\GIBZ Stuff\00_PROJEKTARBEIT\pojektarbeit\src\client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map