const config = require('../config');
const jwt = require('jsonwebtoken');

// Check the Bearer Header
module.exports = (req, res, next) => {

    // Cookie approach
    if (req.cookies && req.cookies.SESSIONID) {
        try {
            const decoded = jwt.verify(req.cookies.SESSIONID, config.jwtSecretKey);
            req.user = decoded;
            return next();
        } catch (err) {
            return res.status(401).json({
                message: 'Unauthorized! Information sent to FBI.'
            });
        }
    } else {
        return res.status(401).json({
            message: 'Unauthorized! Information sent to FBI.'
        });
    }
}