const config = require('../config');
const jwt = require('jsonwebtoken');

// Check the Bearer Header
module.exports = (role) => {
    return function(req, res, next) {
        if (req.user && req.user.Role == role) {
            next();
        } else {
            return res.status(401).json({
                message: 'Unauthorized! Information sent to FBI.'
            })
        }
    }
}
