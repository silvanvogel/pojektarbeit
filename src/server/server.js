const express = require('express');
const helmet = require('helmet');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const app = express();
const authController = require('./auth/auth.controller');
const tfaController = require('./tfa/tfa.controller');
const postController = require('./post/post.controller');
const db = require('./db/db');
const port = process.env.PORT || 3000;

// Init db
db.initSchema().then();
db.seed().then();

app.use(cookieParser());
app.use(helmet.contentSecurityPolicy({
    directives: {
        frameAncestors: ["'none'"]
    }
}))
app.use(helmet.xssFilter());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use('*', (req, res, next) => {
    console.log(`${req.method} ${req.originalUrl} called`);
    next();
})
app.use('/api/auth', authController);
app.use('/api/tfa', tfaController);
app.use('/api/post', postController);

distributeClient(app);

app.listen(port, () => console.log('Ayee i started on localhost:' + port));

function distributeClient(app) {
    app.use(express.static(path.join(__dirname, '../client-dist/')));
    app.get('*', (req, res, next) => {
        if (req.path.startsWith('/api')) {
            next();
        } else {
            res.sendFile(path.join(__dirname, '../client-dist/index.html'));
        }
    })
}