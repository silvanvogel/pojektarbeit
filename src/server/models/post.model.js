class Post {
    constructor(uid, username, title, content, date, status) {
        this.Uid = uid;
        this.Username = username;
        this.Title = title;
        this.Content = content;
        this.Date = date;
        this.Status = status;
    }
}

module.exports = Post;
