class User {
    constructor(name, username, phone, phoneConfirmed) {
        this.Name = name;
        this.Username = username;
        this.Phone = phone;
        this.PhoneConfirmed = phoneConfirmed;
    }
}

module.exports = User;
