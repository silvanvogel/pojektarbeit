const router = require('express').Router();
const db = require('../db/db');
const authGuard = require('../helpers/auth.guard');
const fetch = require('node-fetch');

// Generate a new jwtSecretKey and return the qr code
router.post('/setup', authGuard, async (req, res) => {
    let phoneNumber = req.body.phone;

    if (phoneNumber) {
        await db.knex('User').update({
            Phone: phoneNumber,
            PhoneConfirmed: false
        }).where('Username', req.user.Username);

        const body = { 
            recipient: phoneNumber, 
            length: 4, 
            flash: false 
        };
        
        const response = await fetch('https://europe-west1-gibz-informatik.cloudfunctions.net/send_2fa_sms', 
            {
                method: 'post',
                body: JSON.stringify(body),
                headers: { 'Content-Type': 'application/json', 'Authorization': 'MTlfMjAubTE4My5zdm9nZWw=' }
            });

        // if request was successful, safe the (new) token
        if (response.status == 200) {
            const data = await response.json();

            await db.knex('Token').delete().where('UserId', req.user.UserId);
            
            // New token valid for 5mins
            await db.knex('Token').insert({
                UserId: req.user.UserId,
                Token: data.token,
                Expiry: new Date(new Date().getTime() + 30000).getTime()
            });

            return res.status(200).json({
                message: 'Successful! Please verify your token.'
            });
        } else {
            return res.status(400).json({ 
                message: "Phone number invalid!"
            });
        }
    } else {
        return res.status(400).json({ 
            message: "No phone number provided"
        });
    }
});

router.post('/setup/token', authGuard, async (req, res) => {
    const { token } = req.body;
    const tokenRow = (await db.knex('Token').select('*').where('UserId', req.user.UserId))[0];
    if (tokenRow && tokenRow.Token == token && tokenRow.Expiry > new Date().getTime()) {
        await db.knex('User').update({
            PhoneConfirmed: true
        }).where('Username', req.user.Username);

        return res.status(200).json({
            message: 'TFA successfully enabled'
        });
    }
    return res.status(400).json({
        message: 'Token could not be verified'
    });
});

router.delete('/', authGuard, async (req, res) => {
    await db.knex('User').update({
        PhoneConfirmed: false,
        Phone: null
    }).where('Id', req.user.UserId);

    return res.status(200).json({
        message: 'Successfully disabled TFA!'
    });
});

module.exports = router;