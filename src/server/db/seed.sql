INSERT INTO User 
(Name, Username, Password, Salt, Role)
VALUES 
('DefaultAdmin', 'defaultadmin', '$2a$08$KeBysG0Nzi0P8Iv8.AhfWeD3sErawe0hfgrHEjAvpDXMjyyoRvyqG', '$2a$08$KeBysG0Nzi0P8Iv8.AhfWe', 'Admin')
°
INSERT INTO User 
(Name, Username, Password, Salt, Role)
VALUES 
('DefaultUser', 'defaultuser', '$2a$08$CiYsVtlVK/3kATDS8toIL.qelpwPoeUL/DbnlULGzYgjwN8kKybOq', '$2a$08$CiYsVtlVK/3kATDS8toIL.', 'User')