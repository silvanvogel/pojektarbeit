const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');

let database;

class Database {
    constructor() {
        this.dbPath = __dirname + '/data.db';
        this.initSchemaPath = __dirname + '/initSchema.sql';
        this.seedPath = __dirname + '/seed.sql';

        this.knex = require('knex')({
                client: 'sqlite3',
                connection: () => ({
                    filename: this.dbPath
                }),
                useNullAsDefault: true
        });
        console.log('Successfully connected to db');
    }

    async initSchema() {
        if (this.knex) {
            try{
                const dbSchemaTables = fs.readFileSync(this.initSchemaPath).toString().split('°');

                await (this.knex.transaction(async transaction => {
                    for (const talbe of dbSchemaTables) {
                        await transaction.raw(talbe);
                    }
                }));
                console.log('Schema successfully created');
            }
            catch(ex){
                console.log(ex);
                return;
            }
        }
    }

    async seed() {
        if (this.knex) {
            try{
                const data = fs.readFileSync(this.seedPath).toString().split('°');
            
                await (this.knex.transaction(async transaction => {
                    for (const insert of data) {
                        await transaction.raw(insert);
                    }
                })); 
                console.log('Seed successfully inserted');
            } catch(ex){
                console.log(ex);
                return;
            }
        }
    }

    dispose() {
    }
}

module.exports = database ? database : new Database();

