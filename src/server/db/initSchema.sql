CREATE TABLE IF NOT EXISTS User 
(
  Id integer primary key AUTOINCREMENT, 
  Name varchar(50) NOT NULL, 
  Username varchar(30) NOT NULL UNIQUE,
  Password text NOT NULL,
  Salt text NOT NULL,
  Phone text,
  PhoneConfirmed bit DEFAULT 0,
  Role text NOT NULL,
  MD_Active NOT NULL DEFAULT 1
);
°
CREATE TABLE IF NOT EXISTS Token 
(
  Id integer primary key AUTOINCREMENT,
  UserId integer NOT NULL,
  Token text NOT NULL,
  Expiry bigint,
  MD_Active NOT NULL DEFAULT 1,
  FOREIGN KEY (UserId) REFERENCES User(Id)
);
°
CREATE TABLE IF NOT EXISTS Post 
(
  Id integer primary key AUTOINCREMENT,
  UID text NOT NULL,
  UserId integer NOT NULL,
  Title varchar(100) NOT NULL,
  Content varchar(1000) NOT NULL,
  ImageURL text,
  Date timestamp DEFAULT CURRENT_TIMESTAMP,
  Status integer NOT NULL DEFAULT 0,
  MD_Active NOT NULL DEFAULT 1,
  FOREIGN KEY (UserId) REFERENCES User(Id)
);
°
CREATE TABLE IF NOT EXISTS Comment 
(
  Id integer primary key AUTOINCREMENT,
  UserId integer NOT NULL,
  PostId integer NOT NULL,
  Content varchar(1000) NOT NULL,
  Date timestamp DEFAULT CURRENT_TIMESTAMP,
  MD_Active NOT NULL DEFAULT 1,
  FOREIGN KEY (UserId) REFERENCES User(Id),
  FOREIGN KEY (PostId) REFERENCES Post(Id)
);
