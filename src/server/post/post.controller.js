const router = require('express').Router();
const authGuard = require('../helpers/auth.guard');
const hasRole = require('../helpers/hasRole.guard');
const user = require('../helpers/user.getter');
const db = require('../db/db');
const uuid = require('uuid/v4');

// Get all posts
router.get('/', async (req, res) => {
    const posts = await db.knex('Post')
        .select('UID', 'Title', 'Content', 'Date', 'Status', 'Post.MD_Active', 'User.Username')
        .innerJoin('User', 'User.Id', 'Post.UserId')
        .where('Post.MD_Active', 1)
        .andWhere('Status', 1)
        .orderBy('Date', 'DESC');

    return res.status(200).json(posts);
});

// Get all posts from looged in user
router.get('/my', authGuard, async (req, res) => {
    const posts = await db.knex('Post')
        .select('UID', 'Title', 'Content', 'Date', 'Status', 'Post.MD_Active')
        .where('MD_Active', 1)
        .andWhere('UserId', req.user.UserId)
        .orderBy('Date', 'DESC');

    return res.status(200).json(posts);
});

// Get all posts for admin dashboard
router.get('/admin', authGuard, hasRole('Admin'), async (req, res) => {
    const posts = await db.knex('Post')
        .select('UID', 'Title', 'Content', 'Date', 'Status', 'Post.MD_Active')
        .orderBy('Date', 'DESC');

    return res.status(200).json(posts);
});

// Get post by uid
router.get('/:uid', user, async (req, res) => {
    let post;

    // check if user is admin --> if true, show deleted posts
    if (req.user && req.user.Role === 'Admin') {
        post = (await db.knex('Post')
            .select('UID', 'Title', 'Content', 'Date', 'Post.MD_Active')
            .where('UID', req.params.uid))[0];
    } else {
        post = (await db.knex('Post')
            .select('UID', 'Title', 'Content', 'Date', 'Post.MD_Active')
            .where('MD_Active', 1)
            .andWhere('UID', req.params.uid))[0];
    }
    return res.status(200).json(post);
});

// Get post detail information
router.get('/:uid/detail', user, async (req, res) => {
    let post = (await db.knex('Post')
        .select('UID', 'Status', 'Post.MD_Active', 'User.Username')
        .innerJoin('User', 'User.Id', 'Post.UserId')
        .where('UID', req.params.uid))[0];

    // if user is admin, return post
    if (req.user && req.user.Role === 'Admin') {
        post = (await db.knex('Post').select('*').where('UID', req.params.uid))[0];
    } else  if (req.user) {
        // if user isn't an admin, he can only view the post isn't deleted and active
        if (post.Status === 1 && post.MD_Active === 1) {
            post = (await db.knex('Post')
            .select('Post.Id', 'UID', 'Title', 'Content', 'Date', 'Status', 'Post.MD_Active', 'User.Username')
                .innerJoin('User', 'User.Id', 'Post.UserId')
                .where('Post.MD_Active', 1)
                .andWhere('UID', req.params.uid))[0];
        } else if (post.Username === req.user.Username && post.MD_Active) {
            post = (await db.knex('Post')
            .select('Post.Id', 'UID', 'Title', 'Content', 'Date', 'Status', 'Post.MD_Active', 'User.Username')
                .innerJoin('User', 'User.Id', 'Post.UserId')
                .where('Post.MD_Active', 1)
                .andWhere('UID', req.params.uid))[0];
        } else {
            post = null;
        }
    } else {
        if (post.Status === 1 && post.MD_Active) { 
            post = (await db.knex('Post')
                .select('Post.Id', 'UID', 'Title', 'Content', 'Date', 'Status', 'Post.MD_Active')
                .where('Post.MD_Active', 1)
                .andWhere('UID', req.params.uid))[0];
        } else {
            post = null;
        }
    }

    comments = await db.knex('Comment')
        .select('Comment.Content AS Content', 'Comment.Date AS Date', 'User.Username AS Username')
        .innerJoin('User', 'User.Id', 'Comment.UserId')
        .where('Comment.MD_Active', 1)
        .andWhere('PostId', post && post.Id ? post.Id : 0)
        .orderBy('Date', 'DESC');

    if (post) {
        delete post.Id;
        post.Comments = comments;
    }
    return res.status(200).json(post);
});

// Create post
router.post('/', authGuard, async (req, res) => {
    await db.knex('Post').insert({
        UID: uuid(),
        UserId: req.user.UserId,
        Title: req.body.post.Title,
        Content: req.body.post.Content,
    });
    return res.status(200).json({
        message: 'Post successfully created'
    })
});

// Update post by uid
router.put('/:uid', authGuard, async (req, res) => {
    if (req.user.Role === 'Admin') {
        await db.knex('Post').update({
            Title: req.body.post.Title,
            Content: req.body.post.Content,
        }).where('UID', req.params.uid);
    } else {
        await db.knex('Post').update({
            Title: req.body.post.Title,
            Content: req.body.post.Content,
        }).where('UID', req.params.uid).andWhere('UserId', req.user.UserId);
    }
    return res.status(200).json({
        message: 'Post successfully updated'
    })
});

// Update post status
router.put('/:uid/toggleStatus', authGuard, async (req, res) => {
    if (req.user.Role === 'Admin') {
        const post = (await db.knex('Post').select('Status').where('UID', req.params.uid))[0];
        if (post) {
            await db.knex('Post').update({
                Status: post.Status === 1 ? 0 : 1
            }).where('UID', req.params.uid);
            return res.status(200).json({
                message: 'Post status updated'
            })
        }
    } else {
        return res.status(400).json({
            message: 'Unauthorized! Information sent to FBI.'
        })
    }

});

// Delete post (Actually just sets them on Inactive)
router.delete('/:uid', authGuard, async (req, res) => {
    if (req.user.Role === 'Admin') {
        await db.knex('Post').update({
            MD_Active: false
        }).where('UID', req.params.uid);
    } else {
        await db.knex('Post').update({
            MD_Active: false
        }).where('UID', req.params.uid).andWhere('UserId', req.user.UserId);
    }
   
    return res.status(200).json({
        message: 'Post deleted.'
    });
});

router.post('/comment', authGuard, async (req, res) => {
    const postId = (await db.knex('Post').where('UID', req.body.postUid))[0].Id;
    if (req.body.comment && req.body.comment.length <= 200) {
        await db.knex('Comment').insert({
            PostId: postId,
            UserId: req.user.UserId,
            Content: req.body.comment
        });
        return res.status(200).json({
            message: 'Successfully commented.'
        });
    } 
    return res.status(400).json({
        message: 'Adding comment failed.'
    })
});

module.exports = router;