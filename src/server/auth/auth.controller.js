const router = require('express').Router();
const db = require('../db/db');
const authGuard = require('../helpers/auth.guard');
const jwt = require('jsonwebtoken');
const config = require('../config');
const bcrypt = require('bcryptjs');
const fetch = require('node-fetch');

// User Login
router.post('/login', async (req, res, next) => {
    const { username, password } = req.body;

    const user = (await db.knex('User').select('*').whereRaw('LOWER(Username) = ?', username.toLowerCase()).andWhere('MD_Active', true))[0];
    if (!user) {
        return res.status(400).json({
            message: "Username/password are incorrect!"
        });
    }
    const pw = await bcrypt.hash(password + config.pepper, user.Salt)
    if (user.Password !== pw) {
        return res.status(400).json({
            message: "Username/password are incorrect!"
        });
    }

    // Create User Session
    const generateAndSendJWTToken = (user) => {
        const jwtToken = jwt.sign({
            UserId: user.Id, 
            Username: user.Username,
            Role: user.Role
        }, config.jwtSecretKey, { expiresIn: 60 * 60 });
        
        res.cookie('SESSIONID', jwtToken, { httpOnly: true, sameSite: 'Strict', expires: new Date(new Date().getTime() + 61 * 60 * 1000) })
        return res.status(200).json({
            Expiry: (new Date().getTime()) +  60 * 60 * 1000,
            user: {
                Name: user.Name, 
                Username: user.Username,
                Role: user.Role
            },
        });
    }

    // Check if tfa is enabled
    if (!user.PhoneConfirmed) {
        return generateAndSendJWTToken(user);
    } else {
        // Check if token got submitted
        if (req.headers['x-tfa']) {
            const tokenRow = (await db.knex('Token').select('*').where('UserId', user.Id))[0];
            if (tokenRow && tokenRow.Token == req.headers['x-tfa'] && tokenRow.Expiry > new Date().getTime()) {
                return generateAndSendJWTToken(user);
            }
            return res.status(400).json({
                message: 'Token invalid!'
            });
        } else {
            const body = { 
                recipient: user.Phone, 
                length: 4, 
                flash: false 
            };

            const response = await fetch('https://europe-west1-gibz-informatik.cloudfunctions.net/send_2fa_sms', 
            {
                method: 'post',
                body: JSON.stringify(body),
                headers: { 'Content-Type': 'application/json', 'Authorization': 'MTlfMjAubTE4My5zdm9nZWw=' }
            });
            if (response.status === 200) {
                const data = await response.json();

                // Delete old token
                await db.knex('Token').delete().where('UserId', user.Id);
                // Set new token
                await db.knex('Token').insert({
                    UserId: user.Id,
                    Token: data.token,
                    Expiry: new Date(new Date().getTime() + 5 * 60 * 1000).getTime()
                });

                return res.status(206).json({
                    message: 'Successful! Please verify your Token.'
                });
            } else {
                return res.status(400).json({ 
                    message: "No valid phone number"
                });
            }
        }
    }
});

router.post('/register', async (req, res) => {
    const { name, username, password } = req.body;

    if (!username || !password || (username.trim() == "" || password.trim() == "")) {
        return res.status(400).json({message: 'Username and password are required'});
    }

    if ((await db.knex('User').select('*').count('Id AS count').whereRaw('LOWER(Username) = ?', username.toLowerCase()))[0].count > 0) {
        return res.status(400).json({message: 'Username already taken!'});
    }

    const salt = await bcrypt.genSalt(8);
    const pw = await bcrypt.hash(password + config.pepper, salt);

    await db.knex('User').insert({
        Name: name,
        Username: username,
        Password: pw,
        Salt: salt,
        Role: 'User',
        MD_Active: true
    });

    return res.status(200).json({message: "You successfully registered!"});
});

// GET: Check if username taken
router.get('/isUsernameTaken', async (req, res) => {
    return (await db.knex('User')
        .select('*')
        .count('Id AS count')
        .whereRaw('LOWER(Username) = ?', req.query.u.toLowerCase()))[0].count > 0 ? res.send(true) : res.send(false);
})

// GET: user information
router.get('/me', authGuard, async (req, res) => {
    if (req.user && req.user.Username) {
        const user = (await db.knex('User')
            .select('Name', 'Username', 'Phone', 'PhoneConfirmed', 'Role')
            .where('Username', req.user.Username))[0];
            
        return res.status(200).json(user);
    }
});

// POST: save user information
router.post('/me', authGuard, async (req, res) => {
    if (req.user && req.user.Username && req.body && req.body.user.Name) {
        try {
            
            await db.knex('User').where('Username', req.user.Username)
                .update({
                    Name: req.body.user.Name
                });
            return res.status(200).json({
                message: 'OK'
            });
        } catch (err) {
            return res.status(400).json({
                message: 'Could not save changes!'
            });
        }
    } else {
        return res.status(400).json({
            message: 'Could not save changes! Please leave no inputs blank!'
        });
    }
});

router.post('/logout', async (req, res, next) => {
    res.clearCookie('SESSIONID')
    return res.status(200).json({
        message: 'Successfully logged out'
    })
});

module.exports = router;