import { Comment } from './comment.viewmodel';

export class Post {
    UID: string;
    Username: string;
    Title: string;
    Content: string;
    Date: Date;
    MD_Active: boolean;
    Comments: Array<Comment>;
}
