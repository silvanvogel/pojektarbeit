export class User {
    Name: string;
    Username: string;
    Phone: string;
    PhoneConfirmed: boolean;
    Role: string;
}
