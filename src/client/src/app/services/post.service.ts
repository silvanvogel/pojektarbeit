import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { catchError } from 'rxjs/operators';
import { Post } from '../models/post.viewmodel';

@Injectable({
  providedIn: 'root'
})
export class PostService {

    private baseUrl = 'api/post';

    constructor(
        private http: HttpClient,
        private errorService: ErrorService
    ) { }

    public getPosts() {
        return this.http.get<Array<Post>>(`${this.baseUrl}`)
            .pipe(catchError(this.errorService.handleError));
    }

    public getMyPosts() {
        return this.http.get<Array<Post>>(`${this.baseUrl}/my`)
            .pipe(catchError(this.errorService.handleError));
    }

    public createPost(post: Post) {
        return this.http.post(`${this.baseUrl}`, { post })
            .pipe(catchError(this.errorService.handleError));
    }

    public updatePost(uid: string, post: Post) {
        return this.http.put(`${this.baseUrl}/${uid}`, { post })
            .pipe(catchError(this.errorService.handleError));
    }

    public getPost(uid: string) {
        return this.http.get<Post>(`${this.baseUrl}/${uid}`)
            .pipe(catchError(this.errorService.handleError));
    }

    public getPostDetail(uid: string) {
        return this.http.get<Post>(`${this.baseUrl}/${uid}/detail`)
            .pipe(catchError(this.errorService.handleError));
    }

    /*----Admin START ----*/
    public getPostsAdmin() {
        return this.http.get<Array<Post>>(`${this.baseUrl}/admin`)
            .pipe(catchError(this.errorService.handleError));
    }

    public deletePost(uid: string) {
        return this.http.delete(`${this.baseUrl}/${uid}`)
            .pipe(catchError(this.errorService.handleError));
    }

    public postComment(postUid: string, comment: string) {
        return this.http.post(`${this.baseUrl}/comment`, { postUid, comment })
            .pipe(catchError(this.errorService.handleError));
    }
    
    public toggleStatus(uid: string) {
        return this.http.put(`${this.baseUrl}/${uid}/toggleStatus`, { uid })
            .pipe(catchError(this.errorService.handleError));
    }
    /*----Admin END ----*/
}
