import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ErrorService } from './error.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthService {
    private baseUrlAuth = 'api/auth';
    private baseUrlTfa = 'api/tfa';

    constructor(private http: HttpClient, private errorService: ErrorService) { }

    public register(name: string, username: string, password: string) {
        return this.http.post(`${this.baseUrlAuth}/register`, {name, username, password}, { observe: 'response' })
            .pipe(catchError(this.errorService.handleError));
    }
    
    public login(username: string, password: string, token?: string) {
        let headerOptions = null;
        if (token) {
            headerOptions = new HttpHeaders({
                'x-tfa': token
            });
        }
        return this.http.post(`${this.baseUrlAuth}/login`, { username, password }, { observe: 'response', headers: headerOptions })
            .pipe(catchError(this.errorService.handleError));
    }

    /*---- TFA START ----*/
    public setupTFA(phone: string) {
        return this.http.post(`${this.baseUrlTfa}/setup`, { phone }, { observe: 'response' })
            .pipe(catchError(this.errorService.handleError));
    }

    public verifyAndUpdateToken() {
        return this.http.get(`${this.baseUrlAuth}/verifyAndUpdateToken`, { observe: 'response' })
            .pipe(catchError(this.errorService.handleError));
    }

    public verifyToken(token: any) {
        return this.http.post(`${this.baseUrlTfa}/setup/token`, { token }, { observe: 'response' })
            .pipe(catchError(this.errorService.handleError));
    }

    public disableTFA() {
        return this.http.delete(`${this.baseUrlTfa}`, { observe: 'response' })
            .pipe(catchError(this.errorService.handleError));
    }
    /*---- TFA END ----*/
}
