import { Injectable } from '@angular/core';
import { catchError, map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { User } from '../models/user.viewmodel';
import { Router } from '@angular/router';

@Injectable()
export class UserService {

    public currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    private baseUrl = 'api/auth';

    constructor(
        private authService: AuthService,
        private http: HttpClient,
        private errorService: ErrorService,
        private router: Router
    ) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public isLoggedIn() {
        return localStorage.getItem('Expiry') && parseInt(localStorage.getItem('Expiry'), 10) > new Date().getTime();
    }

    public login(username: string, password: string, token?: string) {
        return this.authService.login(username, password, token)
            .pipe(
                map(data => {
                    if (data && data.status === 200 && data.body['Expiry']) {
                        localStorage.setItem('Expiry', data.body['Expiry']);
                        this.currentUserSubject.next(data.body['user'] as User);
                    }
                    return data;
                }
            ));
    }

    public isUsernameTaken(username: string) {
        return this.http.get<boolean>(`${this.baseUrl}/isUsernameTaken?u=${username}`)
            .pipe(catchError(this.errorService.handleError));
    }

    public isAdmin() {
        if (this.isLoggedIn()) {
            return this.currentUser
                .pipe(
                    map(currentUser =>  {
                        return currentUser && currentUser.Role === 'Admin';
                    })
                );
        }
        return of(false);
    }

    public register(name: string, username: string, password: string) {
        return this.authService.register(name, username, password);
    }

    public getUserData() {
        if (this.isLoggedIn()) {
            return this.http.get(`${this.baseUrl}/me`)
                .pipe(catchError(this.errorService.handleError));
        }
        return null;
    }

    public saveUserData(user: User) {
        return this.http.post(`${this.baseUrl}/me`, { user })
            .pipe(catchError(this.errorService.handleError));
    }

    // Logout the current user
    public logout() {
        return this.http.post(`${this.baseUrl}/logout`, {})
            .pipe(
                tap(() => {
                    this.currentUserSubject.next(null);
                    localStorage.removeItem('Expiry');
                }),
                catchError(this.errorService.handleError)
            );
    }

    // Create new userSubject if the ExpiryDate of 1h is exceeded
    public refreshUserInfo() {
        this.currentUser.subscribe(currentUser => {
            if (!currentUser && this.isLoggedIn()) {
                this.getUserData().subscribe(newUserObject => {
                        console.log('User reloaded');
                        this.currentUserSubject.next(newUserObject as User);
                    }
                );
            }
        });
    }
}
