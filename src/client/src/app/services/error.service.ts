import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

export class ErrorService {

    public handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.log('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.log(
                `Backend returned code ${error.status}, ` +
                `body was: ${JSON.stringify(error.error)}`);
        }

        return throwError(error.error.message ? error.error.message : 'Something bad happened; please try again later.');
    }

    public getClientMessage(error: Error): string {
        if (!navigator.onLine) {
            return 'No Internet Connection';
        }
        return error.message ? error.message : error.toString();
    }

    public getClientStack(error: Error): string {
        return error.stack;
    }

    public getServerMessage(error: HttpErrorResponse): string {
        return error.error && error.error.message ? error.error.message : error.message;
    }

    public getServerStack(error: HttpErrorResponse): string {
        // handle stack trace
        return 'stack';
    }
}
