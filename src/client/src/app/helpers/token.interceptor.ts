import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { Injectable } from '@angular/core';


/**
 * Token inteceptor when token is stored in local storage
 * Switched my approach to Session cookies instead, so this class is obsolete
 * Leaving it here for reference
 *
 * @export
 * @class TokenInterceptor
 * @implements {HttpInterceptor}
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private userService: UserService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (this.userService.isLoggedIn()) {
            const modified = req.clone({
                setHeaders: { Authorization: `Bearer ${localStorage.getItem('access_token')}` }
            });
            return next.handle(modified);
        }

        return next.handle(req);
    }
}
