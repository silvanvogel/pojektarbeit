import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';
import { MatSnackBar } from '@angular/material';
import { pipe, concat, merge } from 'rxjs';
import { map, switchMap, switchMapTo, concatMap } from 'rxjs/operators';


@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
            private userService: UserService,
            private router: Router,
            private snackBar: MatSnackBar
        ) {
    }
    canActivate(nextRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const canActiveFunction = (currentUser) => {
            if (nextRoute.data['Role']) {
                if (currentUser && currentUser['Role'] === nextRoute.data['Role']) {
                    return true;
                } else {
                    this.router.navigate(['/']);
                    this.snackBar.open('You aren\'t allowed to view this page!', 'Close', { panelClass: ['error', 'mat-warn']});
                    return false;
                }
            } else {
                if (this.userService.isLoggedIn() && !nextRoute.data['Role']) {
                    return true;
                } else {
                    this.router.navigate(['/login']);
                    return false;
                }
            }
        };

        // If value is empty, first try reloading it and then check again
        if (!this.userService.currentUserSubject.value) {
            return this.userService.getUserData().pipe(
                map(currentUser => {
                    return canActiveFunction(currentUser);
                })
            );
        } else {
            return canActiveFunction(this.userService.currentUserSubject.value);
        }
    }
}
