import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/models/post.viewmodel';
import { PostService } from 'src/app/services/post.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { ConfirmComponent } from '../../shared/confirm/confirm.component';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {

    public myPosts = new Array<Post>();
    public displayedColumns = ['index', 'title', 'date', 'status', 'edit'];
    constructor(
        private postService: PostService,
        private snackBar: MatSnackBar,
        private dialog: MatDialog,
        public userService: UserService
    ) { }

    ngOnInit() {
        this.getMyPosts();
    }

    getMyPosts() {
        this.postService.getMyPosts().subscribe(posts => {
            this.myPosts = posts;
        }, err => {
            this.snackBar.open('Failed loading the user data!', 'Close', { panelClass: ['error', 'mat-warn']});
        });
    }

    deletePost(event, uid) {
        event.stopPropagation();
        const dialogRef = this.dialog.open(ConfirmComponent);
        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.postService.deletePost(uid).subscribe(posts => {
                    this.getMyPosts();
                    this.snackBar.open('Successfully deleted post!', 'Close');
                }, err => {
                    this.snackBar.open('Failed deleting the post!', 'Close', { panelClass: ['error', 'mat-warn']});
                });
            }
        });
    }

    convertToString(str) {
        return new Date(str).toLocaleString('de-Ch');
    }
}
