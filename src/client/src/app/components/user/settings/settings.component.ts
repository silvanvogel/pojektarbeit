import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { User } from 'src/app/models/user.viewmodel';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

    userInfo: User;

    phoneForm = new FormGroup({
        phone: new FormControl('', [Validators.required])
    });

    tokenForm = new FormGroup({
        token: new FormControl('', [Validators.required])
    });

    tfaActive = false;

    constructor(
        private userService: UserService,
        private authService: AuthService,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit() {
        this.getUserData();
    }

    private getUserData() {
        this.userService.getUserData().subscribe(userInfo => {
            this.userInfo = userInfo as User;
            this.phoneForm.patchValue({phone: this.userInfo.Phone});
        }, err => {
            this.snackBar.open('Couldnt load the user data!', 'Close', { panelClass: ['error', 'mat-warn']});
        });
    }

    public submitPhoneForm() {
        if (this.phoneForm.valid) {
            this.authService.setupTFA(this.phoneForm.controls.phone.value).subscribe(res => {
                if (res.status === 200) {
                    this.tfaActive = true;
                    this.getUserData();
                    this.snackBar.open('Successfully sent token! Please check your phone and verify it!', 'Close');
                } else {
                    this.snackBar.open('Failed sending the token! Please check your phone number.', 'Close',
                        { panelClass: ['error', 'mat-warn']});
                }
            });
        }
    }

    public saveUserData() {
        this.userService.saveUserData(this.userInfo).subscribe(() => {
            this.getUserData();
            this.snackBar.open('Data successfully saved!', 'Close');
        }, err => {
            this.snackBar.open('Failed saving the user data!', 'Close', { panelClass: ['error', 'mat-warn']});
        });
    }

    public disableTFA() {
        this.authService.disableTFA().subscribe(res => {
            this.tfaActive = false;
            this.getUserData();
            this.snackBar.open('Successfully disabled tfa!', 'Close');
        }, err => {
            this.snackBar.open('Failed disabling tfa!', 'Close',
                { panelClass: ['error', 'mat-warn']});
        });
    }

    public submitTokenForm() {
        if (this.tokenForm.valid) {
            this.authService.verifyToken(this.tokenForm.controls.token.value).subscribe(res => {
                if (res.status === 200) {
                    this.tfaActive = false;
                    this.getUserData();
                    this.snackBar.open('Successfully enabled tfa!', 'Close');
                } else {
                    this.snackBar.open('Failed enabling tfa! Please enter the correct token.', 'Close',
                        { panelClass: ['error', 'mat-warn']});
                }
            });
        }
    }
}
