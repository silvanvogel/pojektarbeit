import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Post } from 'src/app/models/post.viewmodel';
import { PostService } from 'src/app/services/post.service';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

    isEditMode = false;
    uid: string;

    form: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private postService: PostService,
        private snackBar: MatSnackBar,
        public location: Location
    ) { }

    ngOnInit() {
        this.form = new FormGroup({
            Title: new FormControl('', [Validators.required]),
            Content: new FormControl('', [Validators.required])
        });

        this.route.params.subscribe(params => {
            if (params['uid']) {
                // edit
                this.uid = params['uid'];
                this.getExistingPost(params['uid']);
                this.isEditMode = true;
            } else {
                // create
                this.form.patchValue(new Post());
            }
        });
    }

    getExistingPost(uid: string) {
        this.postService.getPost(uid).subscribe(post => {
            this.form.patchValue(post);
        }, err => {
            this.snackBar.open('Post loading', 'Close', { panelClass: ['error', 'mat-warn']});
        });
    }

    createPost() {
        if (this.form.valid) {
            this.postService.createPost(this.form.getRawValue() as Post).subscribe(() => {
                this.router.navigate(['/user/dashboard']);
                this.snackBar.open('Post creation successful', 'Close');
            }, err => {
                this.snackBar.open('Post creation failed', 'Close', { panelClass: ['error', 'mat-warn']});
            });
        }
    }

    savePost() {
        if (this.form.valid) {
            this.postService.updatePost(this.uid, this.form.getRawValue() as Post).subscribe(() => {
                this.getExistingPost(this.uid);
                this.snackBar.open('Post successfully saved!', 'Close');
            }, err => {
                this.snackBar.open('Post saving failed', 'Close', { panelClass: ['error', 'mat-warn']});
            });
        }
    }

}
