import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { PostService } from 'src/app/services/post.service';
import { ActivatedRoute } from '@angular/router';
import { Post } from 'src/app/models/post.viewmodel';
import { MatSnackBar } from '@angular/material';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

    public uid: string;
    public post: Post;
    public commentForm = new FormGroup({
        comment: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(200)])
    });

    constructor(
        public location: Location,
        private postService: PostService,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        public userService: UserService
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['uid']) {
                this.uid = params['uid'];
                this.getPostDetail();
            }
        });
    }
    
    getPostDetail() {
        this.postService.getPostDetail(this.uid).subscribe(post => {
            this.post = post;
        }, err => {
            this.snackBar.open('Post creation failed', 'Close', { panelClass: ['error', 'mat-warn']});
        });
    }


    postComment() {
        if (this.commentForm.valid) {
            this.postService.postComment(this.uid, this.commentForm.controls.comment.value).subscribe(() => {
                this.commentForm.controls.comment.setValue('');
                this.getPostDetail();
            }, err => {
                this.snackBar.open('Post comment failed', 'Close', { panelClass: ['error', 'mat-warn']});
            });
        }
    }

    convertToString(str) {
        return new Date(str).toLocaleString('de-Ch');
    }

}
