import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ErrorStateMatcher, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    showAfterTouched = new ShowAfterTouchedErrorStateMatcher();
    showImmediate = new ShowImmediateErrorStateMatcher();
    form: FormGroup = new FormGroup({
        name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
        username: new FormControl('', [Validators.required, Validators.maxLength(20)], this.usernameAlreadyTaken.bind(this)),
        password: new FormControl('', [Validators.required]),
        confirmPassword: new FormControl(''),
    }, [this.checkPasswords]);

    constructor(
        private router: Router,
        private userService: UserService,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit() {}

    public submit() {
        if (this.form.valid) {
             this.userService.register(this.form.controls.name.value, this.form.controls.username.value, this.form.controls.password.value)
                .subscribe(data => {
                    if (data.status === 200) {
                        this.router.navigate(['/login']);
                      }
                    if (data.status === 206) {
                        // this.tfaFlag = true;
                    }
                }, err => {
                    this.snackBar.open('Register failed!', 'Close', { panelClass: ['error', 'mat-warn']});
                });
        }
    }

    public hasError(controlName: string, errorName: string) {
        return this.form.controls[controlName].hasError(errorName);
    }

    public checkPasswords(group: FormGroup) {
        const pass = group.get('password').value;
        const confirmPass = group.get('confirmPassword').value;
        return pass === confirmPass ? null : { notSame: true };
    }

    public usernameAlreadyTaken(control: AbstractControl) {
        return this.userService.isUsernameTaken(control.value)
            .pipe(
                map(res => {
                    return res ? { usernameTaken: true } : null;
                })
            );
    }
}

export class ShowAfterTouchedErrorStateMatcher implements ErrorStateMatcher {
    public isErrorState(control: FormControl | null, form: any): boolean {
        const controlBeenTouched = !!(control && (control.touched));
        const parentHasNotSameError = !!(control && control.parent && control.parent.hasError('notSame'));
        return controlBeenTouched && parentHasNotSameError;
    }
}

export class ShowImmediateErrorStateMatcher implements ErrorStateMatcher {
    public isErrorState(control: FormControl | null, form: any): boolean {
        const controlBeenTouched = !!(control && control.invalid && (control.dirty || control.touched));
        return controlBeenTouched;
    }
}
