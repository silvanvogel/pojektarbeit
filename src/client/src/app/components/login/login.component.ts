import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

    public form: FormGroup = new FormGroup({
        username: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required])
    });

    public tokenForm = new FormGroup({
        token: new FormControl('', [Validators.required])
    });

    tfaActive = false;

    constructor(
        private router: Router,
        private userService: UserService,
        private snackBar: MatSnackBar
    ) { }


    public submit() {
        if ((this.form.valid && !this.tfaActive) || (this.tokenForm.valid && this.tfaActive)) {
            this.userService.login(this.form.controls.username.value, this.form.controls.password.value,
                this.tfaActive ? this.tokenForm.controls.token.value : null)
                .subscribe(data => {
                    if (data.status === 200) {
                        this.userService.currentUser.subscribe(user => {
                            if (user && user.Role === 'Admin') {
                                this.router.navigate(['/admin/dashboard']);
                            } else {
                                this.router.navigate(['/user/dashboard']);
                            }
                        });
                      }
                    if (data.status === 206) {
                        this.tfaActive = true;
                    }
                }, err => {
                    this.snackBar.open('Login failed!', 'Close', { panelClass: ['error', 'mat-warn']});
                });
        }
    }

    public hasError(controlName: string, errorName: string) {
        return this.form.controls[controlName].hasError(errorName);
    }
}
