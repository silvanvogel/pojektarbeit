import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/post.viewmodel';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    public allPosts: Array<Post>;

    constructor(
        public userService: UserService,
        private postService: PostService,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit() {
        this.getAllPosts();
    }

    getAllPosts() {
        this.postService.getPosts().subscribe(posts => {
            this.allPosts = posts;
        }, err => {
            this.snackBar.open('Failed fetching the posst!', 'Close', { panelClass: ['error', 'mat-warn']});
        });
    }

}
