import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(
        private router: Router,
        public userService: UserService
    ) {}

    ngOnInit() {
        this.userService.refreshUserInfo();
    }

    logout() {
        this.userService.logout().subscribe(() => {
            this.router.navigate(['/login']);
        });
    }
}
