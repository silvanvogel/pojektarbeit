import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserDashboardComponent } from './components/user/dashboard/dashboard.component';
import { SettingsComponent } from './components/user/settings/settings.component';
import { AuthService } from './services/auth.service';
import { ErrorService } from './services/error.service';
import { UserService } from './services/user.service';
import { DetailComponent } from './components/post/detail/detail.component';
import { EditComponent } from './components/post/edit/edit.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './helpers/auth.guard';
import { ConfirmComponent } from './components/shared/confirm/confirm.component';
import { AdminDashboardComponent } from './components/admin/dashboard/dashboard.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        UserDashboardComponent,
        AdminDashboardComponent,
        SettingsComponent,
        DetailComponent,
        EditComponent,
        ConfirmComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MaterialModule
    ],
    providers: [
        AuthService,
        ErrorService,
        UserService,
        AuthGuard
    ],
    entryComponents: [ConfirmComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
