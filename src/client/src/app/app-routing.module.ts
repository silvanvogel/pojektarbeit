import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserDashboardComponent } from './components/user/dashboard/dashboard.component';
import { SettingsComponent } from './components/user/settings/settings.component';
import { AuthGuard } from './helpers/auth.guard';
import { EditComponent } from './components/post/edit/edit.component';
import { DetailComponent } from './components/post/detail/detail.component';
import { AdminDashboardComponent } from './components/admin/dashboard/dashboard.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'user/dashboard', component: UserDashboardComponent, canActivate: [AuthGuard] },
    { path: 'user/settings', component: SettingsComponent, canActivate: [AuthGuard] },
    { path: 'admin/dashboard', component: AdminDashboardComponent, data: { Role: 'Admin' }, canActivate: [AuthGuard]},
    { path: 'post/edit', component: EditComponent, canActivate: [AuthGuard] },
    { path: 'post/edit/:uid', component: EditComponent, canActivate: [AuthGuard] },
    { path: 'post/detail/:uid', component: DetailComponent},
    { path: '**', redirectTo: ''}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
