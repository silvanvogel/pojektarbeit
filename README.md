# M183 Projektarbeit - Silvan Vogel

### TODO after downloading the project 
> cd .\src\client
> npm install
> ng build

> cd .\src\server
> npm install
> node server.js

Start Browser: localhost:3000

### User/Admin Login

| Rolle |   Username   |   Passwort   |
|-------|--------------|--------------|
| Admin | DefaultAdmin | DefaultAdmin |
| User  | DefaultUser  | DefaultUser  |

  
### Dokumentation
Das Backend sowohl wie das Frontend sind in TypeScript implementiert. Dabei verwendet es Node.js als serverseitige Plattform und Angular als Front-End Webapplikationsframework.

Die M183 Projektarbeit ist eine Single Page Application. Dies wird durch Angular ermöglicht. 
Dabei muss die Seite nicht bei jedem Server Request neu geladen werden, sondern sie beim Start geladen und danach holt es sich die Daten asynchron.


## Server - Technologien
- NodeJS | https://nodejs.org
- ExpressJS | https://expressjs.com
- KnexJS | https://knexjs.org
- SQLite | https://sqlite.org

## Client - Technologien
- Angular 8 | https://angular.io


### Bibliotheken
Hier werden die verwendeten Bibliotheken sowie ihren Nutzen erklärt. 
  
### Server

## ExpressJS
Express.js ist ein serverseitiges Webframework für Node.js. Es vereinfacht das gestalten von modernen Webanwendungen und benötigt mindestens eine Datei - hier server.js - welche Middleware und Controller registriert und den Webserver startet.

## KnexJS
Knex.js ist ein Querybilder für SQLite, MSSQL, MySQL und andere Abfragesprachen. Es ist eine vielversprechende Schnittstelle für eine saubere asynchrone Ablaufsteuerung. Mit Knex.js muss man sich auch nicht mehr um allfällige SQL Injections kümmern (sofern man diese nicht mit knex.raw() erstellt). Parameter und der query-string werden separat and den Datenbanktreiber übergeben.
  
## SQlite 
Sqlite ist eine gemeinfreie Programmbiliothek, welche ein relationales Datenbanksystem enthält. Es unterstützt einen Grossteil der Standard SQL-Befehle und die Datenbank besteht aus einer einzigen Datei. 

## JWT - JsonWebToken
Nach dem Anmelden erhält der Benutzer einen JWT. Dieser wird aber nie beim beim Benutzer selbst gespeichert, sondern wird nur über HTTP als JSON Objekt übergeben. Dieser Information kann überprüft und als vertrauenswürdig eingestuft werden, da sie digital signiert sind.

## Bcrypt
BcryptJS ist eine unterstützt verschiedene Funktionen zum Hashen und zur Salt-Generierung. Somit werden die Passwörter nicht in plain text gespeichert und dadurch die Sicherheit erhöht. Es wendet einen aufwändigen Hashing Algorithmus an welcher. Das heisst, dass das Passwort mehrmals Hash, was den Vorgang verlangsamt, dies ist eine Gegenmassnahme für Bruteforcing. 

Die Passwörter werden mit Salt und Pepper gesichert und als Hash in der Datenbank gespeichert. Somit wird das gleiche Passwort verschiedene Werte oder Hashes erzeugen. Somit sind auch Rainbowtables unbrauchbar, da diese für bekannte Hash-Algorithmen (ohne Salt) erzeugt wurden. Durch die Benützung eines Peppers, ist das Passwort an sich nutzlos, da er den zugehörigen Pepper benötigt. 

## Helmet
Helmet unterstützt Express-Apps zu schützen, indem verschiedene HTTP-HEader festgelegt werden. 
"Es ist keine Wunderwaffe, aber es kann helfen". 

# Content-Security-Policy - (helmet.contentSecurityPolicy());
Damit wird die Content Security Policy gesetzt. Dies soll Clickjacking verhindern. 

# X-XSS-Protection - app.use(helmet.xssFilter());
Der XSS-Filter Soll vor XSS Attacken schützen.

# X-Frame-Options - app.use(helmet.frameguard());
Frameguard schützt die Seite vor Clickjacking durch Iframes.

# X-Powered-By - app.use(helmet.hidePoweredBy());
Dies entfernt den 'X-Powered-By' Header und schützt somit die Serverseitigen Technologien. 

## UUID
Ein Post erhält eine UUID anstatt eine iterative Id. Dies soll URL-Tampering erschweren.


### Client

## Angular 8
Angular ist ein TypeScript basiertes Front-End-Webapplikationsframework. Es verwendet eine Hierarchie von Komponent als zentrales Architekturkonzept und ist modularisiert, womit der Kern leichgteichtiger und schneller wird. 
  

## Angular Material
Angular Material entspricht modernen Webdesign-Prinzipien wie Browser-Portabilität und Geräteunabhängigkeit. Es hilft bei der Erstellung schneller Webseiten und ist vom Google Material Design inspiriert. Dies ist zum Beispiel eine Alternative zu Bootstrap.